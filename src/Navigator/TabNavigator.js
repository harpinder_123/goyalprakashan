import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Image, StyleSheet} from 'react-native';
import Home from '../screens/Home';
import ActiveBooks from '../screens/ActiveBooks';
import Search from '../screens/Search';
import MyAccount from '../screens/MyAccount';

const iconPath = {
  h: require('../images/home.png'),
  ha: require('../images/home1.png'),
  s: require('../images/online1.png'),
  sa: require('../images/online.png'),
  f: require('../images/search1.png'),
  fa: require('../images/search.png'),
  p: require('../images/menu1.png'),
  pa: require('../images/menu.png'),
};

const Tab = createBottomTabNavigator();
const TabIcon = source => <Image source={source} style={styles.tabIcon} />;

const TabNavigator = () => {
  return (
    <Tab.Navigator
      initialRouteName="Home"
      tabBarOptions={{
        keyboardHidesTabBar: true,
        labelStyle: {
          paddingBottom: 2,
          fontSize: 10,
          fontFamily: 'Avenir-Roman',
          fontWeight: '400',
        },
        activeTintColor: '#0253B3',
        activeBackgroundColor: '#FFFFFF',
        inactiveBackgroundColor: '#FFFFFF',
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.h : iconPath.ha),
          headerShown: false,
        }}
      />

      <Tab.Screen
        name="ActiveBooks"
        component={ActiveBooks}
        options={{
          tabBarLabel: 'Online Support',
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.s : iconPath.sa),
          headerShown: false,
        }}
      />

      <Tab.Screen
        name="Search"
        component={Search}
        options={{
          tabBarLabel: 'Search',
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.f : iconPath.fa),
          headerShown: false,
        }}
      />

      <Tab.Screen
        name="MyAccount"
        component={MyAccount}
        options={{
          tabBarLabel: 'Account',
          tabBarIcon: ({focused}) =>
            TabIcon(focused ? iconPath.p : iconPath.pa),
          headerShown: false,
        }}
      />
    </Tab.Navigator>
  );
};
export default TabNavigator;

const styles = StyleSheet.create({
  tabIcon: {
    height: 20,
    width: 20,
    resizeMode: 'contain',
  },
});
