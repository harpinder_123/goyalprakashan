import React from 'react';
import {
  Dimensions,
  Image,
  ImageBackground,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {TextField} from 'react-native-material-textfield';

const {height} = Dimensions.get('window');

export const MainView = props => (
  <SafeAreaView style={{backgroundColor: '#FFFFFF', flex: 1}} {...props} />
);

const mainStyle = StyleSheet.create({
  BottomView: {
    backgroundColor: '#FFFFFF',
    padding: 10,
    marginTop: '125%',
    paddingTop: 30,
    borderTopEndRadius: 50,
    borderTopStartRadius: 50,
  },
});
export const BottomView = props => (
  <View style={mainStyle.BottomView} {...props} />
);

export const ButtonStyle = (
  title,
  bgColor = '#ED6E1E',
  txtcolor = '#FFFFFF',
  onPress,
) => (
  <TouchableOpacity
    activeOpacity={0.6}
    onPress={onPress}
    style={[styles.facebookButton, {backgroundColor: bgColor}]}>
    <Text style={[styles.facebooktext, {color: txtcolor}]}>{title}</Text>
  </TouchableOpacity>
);

const styles = StyleSheet.create({
  facebookButton: {
    backgroundColor: '#3B5998',
    padding: 8,
    borderRadius: 5,
    marginLeft: 15,
  },
  facebooktext: {
    fontFamily: 'AvenirLTStd-heavy',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#FFFFFF',
    alignSelf: 'center',
  },
  containerStyle: {
    width: '90%',
    borderRadius: 10,
    alignSelf: 'center',
    backgroundColor: '#1B172C',
    marginTop: 40,
  },
  inputContainerStyle: {
    marginHorizontal: 20,
    backgroundColor: '#1B172C',
  },
});

export const Header = props => (
  <View style={headerStyle.viewHeader}>
    <View style={headerStyle.flexView}>
      <TouchableOpacity style={headerStyle.touchBack} onPress={props.onPress}>
        <Image
          source={require('../images/back.png')}
          style={headerStyle.imageBack}
        />
      </TouchableOpacity>
      <Text style={headerStyle.textTitle}>{props.title}</Text>
    </View>
  </View>
);

const headerStyle = StyleSheet.create({
  viewHeader: {
    width: '100%',
    backgroundColor: '#fff',
    padding: 15,
  },
  imageBack: {
    width: 12,
    height: 22,
    resizeMode: 'contain',
  },
  touchBack: {
    position: 'absolute',
    left: 0,
  },
  textTitle: {
    fontFamily: 'AvenirLTStd-Medium',
    fontWeight: '500',
    fontSize: 20,
    color: '#000000',
    marginHorizontal: 30,
  },
  flexView: {
    flexDirection: 'row',
    marginTop: 30,
    marginLeft: 10,
    alignItems: 'center',
  },
});

export const Header2 = props => (
  <View style={header2Style.viewHeader}>
    <View style={header2Style.flexView}>
      <TouchableOpacity style={header2Style.touchBack} onPress={props.onPress}>
        <Image
          source={require('../images/back.png')}
          style={header2Style.imageBack}
        />
      </TouchableOpacity>
      <Text style={header2Style.textTitle}>{props.title}</Text>
    </View>
  </View>
);

const header2Style = StyleSheet.create({
  viewHeader: {
    width: '100%',
    backgroundColor: '#fff',
    padding: 15,
    elevation: 8,
  },
  imageBack: {
    width: 12,
    height: 22,
    resizeMode: 'contain',
  },
  touchBack: {
    position: 'absolute',
    left: 0,
  },
  textTitle: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontWeight: 'bold',
    fontSize: 15,
    color: '#000000',
    marginHorizontal: 30,
  },
  flexView: {
    flexDirection: 'row',
    marginTop: 30,
    marginLeft: 10,
    alignItems: 'center',
  },
});

export const HeaderLight = props => (
  <View style={Subheader.flexView}>
    <Image style={Subheader.image} source={require('../images/bg.png')} />
    <TouchableOpacity style={Subheader.touchBack} onPress={props.onPress}>
      <Image source={require('../images/x.png')} style={Subheader.imageBack} />
    </TouchableOpacity>
    <Text style={Subheader.textTitle}>{props.title}</Text>
  </View>
);

const Subheader = StyleSheet.create({
  viewHeader: {
    width: '100%',
    backgroundColor: '#fff',
    padding: 15,
  },
  image: {
    width: 380,
    height: 100,
  },
  imageBack: {
    width: 60,
    height: 60,
    resizeMode: 'contain',
    marginHorizontal: 10,
    marginTop: 35,
  },
  touchBack: {
    position: 'absolute',
    left: 0,
  },
  textTitle: {
    fontFamily: 'AvenirLTStd-Medium',
    fontWeight: '500',
    fontSize: 20,
    color: '#000000',
    marginHorizontal: 30,
  },
  flexView: {
    flexDirection: 'row',
    marginLeft: 10,
    alignItems: 'center',
  },
});
export const HeaderTop = props => (
  <View style={TopHeader.flexView}>
    <ImageBackground
      style={TopHeader.image}
      source={require('../images/bg.png')}>
      <TouchableOpacity style={TopHeader.touchBack} onPress={props.onPress}>
        <Image
          source={require('../images/x.png')}
          style={TopHeader.imageBack}
        />
      </TouchableOpacity>

      <Image
        source={require('../images/book.png')}
        style={TopHeader.imageBook}
      />
    </ImageBackground>

    <Text style={TopHeader.textTitle}>{props.title}</Text>
  </View>
);
const TopHeader = StyleSheet.create({
  viewHeader: {
    width: '100%',
    backgroundColor: '#fff',
    padding: 15,
  },
  image: {
    width: 380,
    height: 340,
  },
  imageBack: {
    width: 60,
    height: 60,
    resizeMode: 'contain',
    marginHorizontal: 10,
    marginTop: 40,
  },
  imageBook: {
    height: 201,
    width: 163,
    alignSelf: 'center',
    borderRadius: 10,
    marginTop: '25%',
  },
  touchBack: {
    position: 'absolute',
    left: 0,
  },
  textTitle: {
    fontFamily: 'AvenirLTStd-Medium',
    fontWeight: '500',
    fontSize: 20,
    color: '#000000',
    marginHorizontal: 30,
  },
  flexView: {
    flexDirection: 'row',
    marginLeft: 10,
    alignItems: 'center',
  },
});

export const HeaderDark = props => (
  <View style={Top2Header.flexView}>
    <ImageBackground
      style={Top2Header.image}
      source={require('../images/screen-video.png')}>
      <TouchableOpacity style={Top2Header.touchBack} onPress={props.onPress}>
        <Image
          source={require('../images/x.png')}
          style={Top2Header.imageBack}
        />
      </TouchableOpacity>
    </ImageBackground>

    <Text style={Top2Header.textTitle}>{props.title}</Text>
  </View>
);
const Top2Header = StyleSheet.create({
  viewHeader: {
    width: '100%',
    backgroundColor: '#fff',
    padding: 15,
  },
  image: {
    width: 400,
    height: height / 2.3,
    marginHorizontal: -10,
    backgroundColor: 'gray',
  },
  imageBack: {
    width: 60,
    height: 60,
    resizeMode: 'contain',
    marginHorizontal: 10,
    marginTop: 40,
  },
  imageBook: {
    height: 201,
    width: 163,
    alignSelf: 'center',
    borderRadius: 10,
    marginTop: '25%',
  },
  touchBack: {
    position: 'absolute',
    left: 0,
  },
  textTitle: {
    fontFamily: 'AvenirLTStd-Medium',
    fontWeight: '500',
    fontSize: 20,
    color: '#000000',
    marginHorizontal: 30,
  },
  flexView: {
    flexDirection: 'row',
    marginLeft: 10,
    alignItems: 'center',
  },
});

export const HeaderImage = props => (
  <View style={Top3Header.flexView}>
    <TouchableOpacity style={Top3Header.touchBack} onPress={props.onPress}>
      <Image source={require('../images/x.png')} style={Top3Header.imageBack} />
    </TouchableOpacity>

    <Text style={Top3Header.textTitle}>{props.title}</Text>
  </View>
);
const Top3Header = StyleSheet.create({
  viewHeader: {
    width: '100%',
    backgroundColor: '#fff',
    padding: 15,
  },
  image: {
    width: 400,
    height: 300,
    marginHorizontal: -10,
  },
  imageBack: {
    width: 60,
    height: 60,
    resizeMode: 'contain',
    marginHorizontal: 10,
    marginTop: 50,
  },
  imageBook: {
    height: 201,
    width: 163,
    alignSelf: 'center',
    borderRadius: 10,
    marginTop: '25%',
  },
  touchBack: {
    position: 'absolute',
    left: 0,
  },
  textTitle: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontWeight: 'bold',
    fontSize: 20,
    color: '#ffffff',
    marginHorizontal: 140,
    marginTop: 50,
  },
  flexView: {
    flexDirection: 'row',
    marginLeft: 10,
    alignItems: 'center',
  },
});

export const HeaderImageDark = props => (
  <View style={Header4.flexView}>
    <TouchableOpacity style={Header4.touchBack} onPress={props.onPress}>
      <Image source={require('../images/x.png')} style={Header4.imageBack} />
    </TouchableOpacity>

    <Text style={Header4.textTitle}>{props.title}</Text>
  </View>
);
const Header4 = StyleSheet.create({
  viewHeader: {
    width: '100%',
    backgroundColor: '#fff',
    padding: 15,
  },
  image: {
    width: 400,
    height: 300,
    marginHorizontal: -10,
  },
  imageBack: {
    width: 60,
    height: 60,
    resizeMode: 'contain',
    marginHorizontal: 10,
    marginTop: 50,
  },
  imageBook: {
    height: 201,
    width: 163,
    alignSelf: 'center',
    borderRadius: 10,
    marginTop: '25%',
  },
  touchBack: {
    position: 'absolute',
    left: 0,
  },
  textTitle: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#ffffff',
    marginHorizontal: 100,
    marginTop: 45,
  },
  flexView: {
    flexDirection: 'row',
    marginLeft: 10,
    alignItems: 'center',
  },
});

export const BottomButton = props => (
  <TouchableOpacity
    activeOpacity={0.8}
    style={bottomStyle.bottomView}
    onPress={props.onPress}>
    <Text style={bottomStyle.textTitle}>{props.bottomtitle}</Text>
  </TouchableOpacity>
);

export const bottomStyle = StyleSheet.create({
  bottomView: {
    width: '95%',
    alignSelf: 'center',
    backgroundColor: '#ED6E1E',
    borderRadius: 20,
    padding: 10,
    marginTop: 60,
    marginBottom: 10,
  },
  textTitle: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontWeight: 'bold',
    fontSize: 18,
    color: '#FFFFFF',
    textAlign: 'center',
  },
});

export const CustomTextField = props => (
  <TextField
    fontSize={18}
    textColor={'#1E2432'}
    tintColor={'grey'}
    containerStyle={{
      backgroundColor: '#FFFFFF',
      marginTop: 20,
      marginHorizontal: 20,
      borderRadius: 10,
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.23,
      shadowRadius: 2.62,
      marginBottom: 10,
      elevation: 4,
    }}
    inputContainerStyle={{marginHorizontal: 20, height: 48}}
    {...props}
  />
);
