import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
const {height} = Dimensions.get('window');

const Question2 = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <ScrollView decelerationRate={0.5}>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity onPress={() => navigation.navigate('EndTest')}>
            <Image
              style={styles.subimage}
              source={require('../images/close.png')}
            />
          </TouchableOpacity>
          <Image
            style={styles.endimage}
            source={require('../images/clock.png')}
          />
          <Text style={styles.text}>10:00</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <View style={styles.container}>
            <Text style={styles.containerText}>2/50</Text>
          </View>
          <Image
            style={styles.end2image}
            source={require('../images/bookmark3.png')}
          />
        </View>
        <Text style={styles.subtext}>
          The information for the preparation{`\n`}of receipt and payments
          account is{`\n`}taken from …
        </Text>
        <View style={styles.box2}>
          <Text style={styles.box2text}>A. Cash Book</Text>
        </View>
        <View style={styles.box}>
          <Text style={styles.boxtext}>B. Income and Expenditure A/c</Text>
        </View>
        <View style={styles.box}>
          <Text style={styles.boxtext}>C. Cash book and balance sheet</Text>
        </View>
        <View style={styles.box}>
          <Text style={styles.boxtext}>D. Revenue Account</Text>
        </View>
        <TouchableOpacity
          style={styles.touch}
          onPress={() => navigation.navigate('TestSummary')}>
          <Text style={styles.touchtext}>Submit Test</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default Question2;

const styles = StyleSheet.create({
  subimage: {
    width: 15,
    height: 15,
    marginTop: 60,
    marginHorizontal: 30,
  },
  endimage: {
    width: 15,
    height: 15,
    marginTop: 60,
    marginLeft: 'auto',
  },
  end2image: {
    width: 16,
    height: 22,
    marginHorizontal: 30,
    marginTop: 20,
    marginLeft: 'auto',
  },
  text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    marginTop: 55,
    marginLeft: 5,
    marginHorizontal: 30,
  },
  container: {
    width: 50,
    height: 30,
    backgroundColor: '#EAEAEA',
    borderRadius: 5,
    marginHorizontal: 30,
    marginTop: 20,
  },
  containerText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#273253',
    textAlign: 'center',
    marginTop: 3,
  },
  images: {
    width: 120,
    height: 110,
    marginTop: 30,
    marginHorizontal: 90,
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 20,
    fontWeight: '500',
    color: '#273253',
    textAlign: 'justify',
    marginTop: 20,
    alignSelf: 'center',
    lineHeight: 30,
  },
  box: {
    padding: 15,
    marginHorizontal: 30,
    backgroundColor: '#ffffff',
    borderRadius: 5,
    elevation: 3,
    marginTop: 20,
  },
  boxtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#333333',
    marginHorizontal: 10,
  },
  box2: {
    padding: 15,
    marginHorizontal: 30,
    backgroundColor: '#ffffff',
    borderRadius: 5,
    // elevation: 3,
    marginTop: 20,
    borderColor: '#0253B3',
    borderWidth: 2,
    backgroundColor: '#cfdcf1',
  },
  box2text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#0253B3',
    marginHorizontal: 10,
  },
  touch: {
    padding: 10,
    marginHorizontal: 30,
    borderRadius: 5,
    backgroundColor: '#0253B3',
    marginTop: '50%',
    elevation: 15,
    marginBottom: 20,
  },
  touchtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 22,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
  },
});
