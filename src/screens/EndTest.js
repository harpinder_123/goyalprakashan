import React, {useState} from 'react';
import {
  Dimensions,
  Image,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

const {height, width} = Dimensions.get('window');
const EndTest = ({navigation}) => {
  const [modalOpen, setModalOpen] = useState(true);

  return (
    <Modal visible={modalOpen} transparent={true}>
      <View style={styles.modal_View}>
        <View style={styles.mdtop}>
          <Text style={styles.text}>
            Are you sure you want{`\n`}to End Test?
          </Text>
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <TouchableOpacity
              style={styles.touch2}
              onPress={() => navigation.navigate('Question')}>
              <Text style={styles.touch2text}>NO</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.touch}
              onPress={() => navigation.navigate('Study2')}>
              <Text style={styles.touchtext}>YES</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    </Modal>
  );
};

export default EndTest;

const styles = StyleSheet.create({
  modal_View: {
    backgroundColor: '#000000aa',
    flex: 1,
  },
  mdtop: {
    backgroundColor: '#FFFFFF',
    marginTop: height / 2.5,
    marginHorizontal: 20,
    borderRadius: 20,
  },
  image: {
    width: 80,
    height: 80,
    alignSelf: 'center',
    marginTop: 20,
  },
  text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 22,
    fontWeight: 'bold',
    color: '#1E2432',
    textAlign: 'center',
    marginTop: 20,
    lineHeight: 30,
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#838383',
    textAlign: 'center',
    marginTop: 20,
    marginBottom: 20,
  },
  touch: {
    width: 132,
    height: 40,
    borderRadius: 25,
    backgroundColor: '#0253B3',
    marginTop: 20,
    marginBottom: 20,
    alignSelf: 'center',
  },
  touch2: {
    width: 132,
    height: 40,
    borderRadius: 25,
    backgroundColor: '#DDDDDD',
    marginTop: 20,
    marginBottom: 20,
    alignSelf: 'center',
  },
  touchtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 8,
  },
  touch2text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#797979',
    marginTop: 8,
  },
});
