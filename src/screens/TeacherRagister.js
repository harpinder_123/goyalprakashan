import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {TextField} from 'react-native-material-textfield';
import {Dropdown} from 'react-native-material-dropdown-v2-fixed';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {useNavigation} from '@react-navigation/native';
const {height} = Dimensions.get('window');
export const CustomInputField = props => (
  <>
    <View style={styles.middleView}>
      <TextField
        containerStyle={styles.tfStyle}
        disableUnderline={true}
        tintColor="#8D92A3"
        {...props}
      />
    </View>
  </>
);
let data = [
  {
    value: 'Student',
  },
  {
    value: 'Teacher',
  },
];

const TeacherRagister = () => {
  const navigation = useNavigation();
  const [userType, setUserType] = useState('');

  const onChangeDropdownUserType = (value, index, data) => {
    setUserType(data[index].value);
    console.log('-----setUserType: ', JSON.stringify(data[index].value));
  };
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />

      <KeyboardAwareScrollView>
        <CustomInputField
          label="Username*"
          //   defaultValue={state.emailphone}
          useNativeDriver={true}
        />
        <CustomInputField
          label="Name*"
          //   defaultValue={state.emailphone}
          useNativeDriver={true}
        />
        <CustomInputField
          label="Email"
          //   defaultValue={state.emailphone}
          useNativeDriver={true}
        />
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            marginHorizontal: 55,
          }}>
          <View>
            <Text style={styles.label}>Code*</Text>
            <Text style={styles.sublabel}>+91</Text>
            <View style={styles.Line} />
          </View>
          <CustomInputField
            label="Mobile*"
            //   defaultValue={state.emailphone}
            useNativeDriver={true}
            keyboardType={'number-pad'}
          />
        </View>
        <CustomInputField
          label="Password"
          //   defaultValue={state.password}
          useNativeDriver={true}
          secureTextEntry={true}
        />
        <CustomInputField
          label="Confirm Password"
          //   defaultValue={state.password}
          useNativeDriver={true}
          secureTextEntry={true}
        />
        <TouchableOpacity
          style={styles.touch}
          onPress={() => navigation.navigate('TeacherOtp')}>
          <Text style={styles.touchtext}>Verify</Text>
        </TouchableOpacity>
        <Text style={styles.bottom}>
          Already have an account?{' '}
          <TouchableOpacity onPress={() => navigation.navigate('Login')}>
            <Text
              style={{
                color: '#FA6400',
                borderBottomWidth: 1,
                borderBottomColor: '#FA6400',
              }}>
              Login
            </Text>
          </TouchableOpacity>
        </Text>
      </KeyboardAwareScrollView>
    </View>
  );
};

export default TeacherRagister;

const styles = StyleSheet.create({
  image: {
    width: 233,
    height: 48,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: 60,
  },
  text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 30,
    fontWeight: 'bold',
    marginTop: 20,
    color: '#1E1F20',
    marginHorizontal: 30,
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 20,
    fontWeight: '600',
    color: '#8F92A1',
    marginHorizontal: 30,
  },
  middleView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  tfStyle: {
    // marginLeft: 30,
    width: '85%',
    marginHorizontal: 40,
  },
  bottomtext: {
    fontFamily: 'AvenirLTStd-Roman',
    fontSize: 16,
    fontWeight: '400',
    color: '#8D92A3',
    marginTop: 10,
    alignSelf: 'flex-end',
    marginHorizontal: 30,
  },
  touch: {
    padding: 10,
    marginHorizontal: 30,
    borderRadius: 5,
    backgroundColor: '#0253B3',
    marginTop: 20,
    elevation: 15,
  },
  touchtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 22,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
  },
  Line: {
    height: 1,
    width: Dimensions.get('window').width / 8,
    borderRadius: 5,
    backgroundColor: '#979797',
    marginTop: 10,
    marginLeft: 35,
  },
  continue: {
    fontFamily: 'nunito',
    fontSize: 15,
    fontWeight: '700',
    color: '#8F92A1',
    marginTop: 35,
    marginLeft: 10,
  },
  drops: {
    backgroundColor: '#fff',
    marginHorizontal: 30,
    marginTop: 20,
  },
  bottom: {
    textAlign: 'center',
    marginTop: 20,
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#8F92A1',
  },
  label: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#8F92A1',
    marginHorizontal: 40,
    marginTop: 12,
  },
  sublabel: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#000000',
    marginHorizontal: 40,
    // marginTop: 3,
  },
});
