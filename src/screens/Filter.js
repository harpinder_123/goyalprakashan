import React, {useState} from 'react';
import {
  View,
  Dimensions,
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
const {height, width} = Dimensions.get('window');
import {RadioButton} from 'react-native-paper';

const Filter = ({navigation}) => {
  const [checked, setChecked] = useState('first');
  return (
    <View style={styles.container}>
      <View style={styles.subContainer}>
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.text}>Filters</Text>
          <TouchableOpacity
            // style={{marginLeft: 'auto', marginHorizontal: 20}}
            onPress={() => navigation.navigate('ExploreBooks')}>
            <Image
              style={styles.image}
              source={require('../images/cross.png')}
            />
          </TouchableOpacity>
        </View>
        <ScrollView>
          <View style={{flexDirection: 'row'}}>
            <View style={styles.back}>
              <TouchableOpacity style={styles.textColor}>
                <Text style={styles.subtext}>Board</Text>
                <Text style={styles.sub2text}>Class</Text>
                <Text style={styles.sub2text}>Subject</Text>
                <Text style={styles.sub2text}>Types of Books</Text>
                <Text style={styles.sub2text}>Author</Text>
              </TouchableOpacity>
            </View>
            <View style={{marginHorizontal: 20, marginTop: 30}}>
              <RadioButton
                value="first"
                status={checked === 'first' ? 'checked' : 'unchecked'}
                onPress={() => setChecked('first')}
                uncheckedColor={'#263238'}
                color={'#FA6400'}
              />
            </View>
            <Text style={styles.radioText}>CBSE</Text>
            <View style={{marginTop: 80, marginLeft: -85}}>
              <RadioButton
                value="second"
                status={checked === 'second' ? 'checked' : 'unchecked'}
                onPress={() => setChecked('second')}
                uncheckedColor={'#263238'}
                color={'#FA6400'}
              />
            </View>
            <Text style={styles.radio2Text}>ICSE</Text>
            <View style={{marginTop: 130, marginLeft: -78}}>
              <RadioButton
                value="third"
                status={checked === 'third' ? 'checked' : 'unchecked'}
                onPress={() => setChecked('third')}
                uncheckedColor={'#263238'}
                color={'#FA6400'}
              />
            </View>
            <Text style={styles.radio3Text}>Punjab Board</Text>
            <View style={{marginTop: 180, marginLeft: -150}}>
              <RadioButton
                value="fourth"
                status={checked === 'fourth' ? 'checked' : 'unchecked'}
                onPress={() => setChecked('fourth')}
                uncheckedColor={'#263238'}
                color={'#FA6400'}
              />
            </View>
            <Text style={styles.radio4Text}>UP Board</Text>
            <View style={{marginTop: 230, marginLeft: -117}}>
              <RadioButton
                value="fivth"
                status={checked === 'fivth' ? 'checked' : 'unchecked'}
                onPress={() => setChecked('fivth')}
                uncheckedColor={'#263238'}
                color={'#FA6400'}
              />
            </View>
            <Text style={styles.radio5Text}>Bihar Board</Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              marginHorizontal: 10,
            }}>
            <TouchableOpacity
              style={styles.touch2}
              onPress={() => navigation.navigate('Accountancy')}>
              <Text style={styles.touch2text}>Reset</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.touch3}
              onPress={() => navigation.navigate('Accountancy')}>
              <Text style={styles.touch3text}>Apply</Text>
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    </View>
  );
};
export default Filter;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#000',
  },
  subContainer: {
    marginTop: height / 2.5,
    backgroundColor: '#ffffff',
    borderRadius: 35,
    width: 360,
    height: 555,
  },
  text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#263238',
    marginTop: 10,
    marginHorizontal: 30,
  },
  image: {
    width: 25,
    height: 25,
    marginTop: 10,
  },
  back: {
    width: 158,
    height: 375,
    backgroundColor: '#f5e4f1',
    marginTop: 20,
  },
  textColor: {
    width: 158,
    height: 40,
    backgroundColor: '#FA6400',
    marginTop: 20,
  },
  subtext: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 7,
    marginHorizontal: 15,
  },
  sub2text: {
    color: '#263238',
    fontSize: 16,
    fontWeight: '500',
    marginTop: 20,
    marginHorizontal: 15,
  },
  radioText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#263238',
    marginTop: 33,
    marginLeft: -15,
  },
  radio2Text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#263238',
    marginTop: 83,
    marginLeft: 5,
  },
  radio3Text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#263238',
    marginTop: 133,
    marginLeft: 5,
  },
  radio4Text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#263238',
    marginTop: 185,
    marginLeft: 5,
  },
  radio5Text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#263238',
    marginTop: 235,
    marginLeft: 5,
  },
  touch2: {
    width: 145,
    height: 50,
    borderRadius: 5,
    backgroundColor: '#6D7278',
    marginTop: '5%',
    elevation: 15,
  },
  touch2text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 17,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 12,
  },
  touch3: {
    width: 186,
    height: 50,
    borderRadius: 5,
    backgroundColor: '#0253B3',
    marginTop: '5%',
    elevation: 15,
  },
  touch3text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 17,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 10,
  },
});
