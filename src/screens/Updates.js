import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderLight} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const Updates = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <HeaderLight onPress={() => navigation.goBack()} />
      <ScrollView decelerationRate={0.5}>
        <Text style={styles.text}>Updates</Text>
        <Text style={styles.profilesubtext}>27 Sep 2021</Text>
        <Text style={styles.profileText}>
          Which Asian countries are US students studying abroad at?
        </Text>
        <ImageBackground
          style={styles.image}
          source={require('../images/Rectangle1.png')}>
          <TouchableOpacity onPress={() => navigation.navigate('Article')}>
            <Image
              style={{
                width: 60,
                height: 60,
                alignSelf: 'center',
                marginTop: 65,
              }}
              source={require('../images/Play1.png')}
            />
          </TouchableOpacity>
        </ImageBackground>
        <View>
          <Text style={styles.profile2subtext}>27 Sep 2021</Text>
          <Text style={styles.profileText}>Popular places in NY</Text>
          <Text style={styles.profile3Text}>
            Use our definitive guide to{`\n`}the top New York{`\n`}attractions
            whenever…
          </Text>
          <Image
            style={styles.endimage}
            source={require('../images/img.png')}
          />
        </View>
        <View>
          <Text style={styles.profile2subtext}>27 Sep 2021</Text>
          <Text style={styles.profileText}>Social Experiment</Text>
          <Text style={styles.profile3Text}>
            A social experiment is the{`\n`}random assignment of{`\n`}human
            subjects to…
          </Text>
          <Image
            style={styles.endimage}
            source={require('../images/img-1.png')}
          />
        </View>
      </ScrollView>
    </View>
  );
};

export default Updates;

const styles = StyleSheet.create({
  image: {
    width: 340,
    height: 200,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: 15,
  },
  text: {
    marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
  },
  profileText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 20,
    fontWeight: '500',
    marginTop: 10,
    color: '#121213',
    marginHorizontal: 30,
    lineHeight: 28,
  },
  profilesubtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 13,
    fontWeight: '500',
    marginTop: 20,
    color: '#FA6400',
    marginHorizontal: 30,
  },
  profile2subtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 13,
    fontWeight: '500',
    marginTop: 40,
    color: '#FA6400',
    marginHorizontal: 30,
  },
  profile3Text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 15,
    fontWeight: '500',
    marginTop: 10,
    color: '#8F92A1',
    marginHorizontal: 30,
  },
  endimage: {
    width: 120,
    height: 120,
    alignSelf: 'flex-end',
    marginHorizontal: 20,
    marginTop: -120,
  },
});
