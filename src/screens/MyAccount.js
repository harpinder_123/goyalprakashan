import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  TextInput,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
const {height} = Dimensions.get('window');

const Search = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <ImageBackground
        style={styles.image}
        source={require('../images/bg.png')}></ImageBackground>
      <Image
        style={{width: 100, height: 100, alignSelf: 'center', marginTop: -20}}
        source={require('../images/gamer.png')}
      />
      <Text style={styles.text}>Pradeep Yadav</Text>
      <Text style={styles.subtext}>12th Grade - CBSE</Text>
      <ScrollView decelerationRate={0.5}>
        <View style={{flexDirection: 'row'}}>
          <Image
            style={{width: 35, height: 35, marginTop: 20, marginHorizontal: 30}}
            source={require('../images/gamer.png')}
          />
          <TouchableOpacity
            onPress={() => navigation.navigate('ProfileDetails')}>
            <Text style={styles.profileText}>My profile</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.Line} />
        <View style={{flexDirection: 'row'}}>
          <Image
            style={{width: 35, height: 35, marginTop: 20, marginHorizontal: 30}}
            source={require('../images/morning.png')}
          />
          <TouchableOpacity onPress={() => navigation.navigate('ActivePlan')}>
            <Text style={styles.profileText}>Active Plan</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.Line} />
        <View style={{flexDirection: 'row'}}>
          <Image
            style={{width: 35, height: 35, marginTop: 20, marginHorizontal: 30}}
            source={require('../images/save.png')}
          />
          <TouchableOpacity>
            <Text style={styles.profileText}>Bookmarks</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.Line} />
        <View style={{flexDirection: 'row'}}>
          <Image
            style={{width: 35, height: 35, marginTop: 20, marginHorizontal: 30}}
            source={require('../images/catlog.png')}
          />
          <TouchableOpacity>
            <Text style={styles.profileText}>Catalogue</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.Line} />
        <View style={{flexDirection: 'row'}}>
          <Image
            style={{width: 35, height: 35, marginTop: 20, marginHorizontal: 30}}
            source={require('../images/test.png')}
          />
          <TouchableOpacity>
            <Text style={styles.profileText}>Checklist</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.Line} />
        <View style={{flexDirection: 'row'}}>
          <Image
            style={{width: 35, height: 35, marginTop: 20, marginHorizontal: 30}}
            source={require('../images/rupee.png')}
          />
          <TouchableOpacity onPress={() => navigation.navigate('Subscription')}>
            <Text style={styles.profileText}>Subscribe</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.Line} />
        <View style={{flexDirection: 'row'}}>
          <Image
            style={{width: 35, height: 35, marginTop: 20, marginHorizontal: 30}}
            source={require('../images/next.png')}
          />
          <TouchableOpacity>
            <Text style={styles.profileText}>Share</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.Line} />
        <View style={{flexDirection: 'row'}}>
          <Image
            style={{width: 35, height: 35, marginTop: 20, marginHorizontal: 30}}
            source={require('../images/contact.png')}
          />
          <TouchableOpacity>
            <Text style={styles.profileText}>Contact us</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.Line} />
        <View style={{flexDirection: 'row'}}>
          <Image
            style={{width: 35, height: 35, marginTop: 20, marginHorizontal: 30}}
            source={require('../images/lock1.png')}
          />
          <TouchableOpacity>
            <Text style={styles.profileText}>Privacy policy</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.Line} />
        <View style={{flexDirection: 'row'}}>
          <Image
            style={{width: 35, height: 35, marginTop: 20, marginHorizontal: 30}}
            source={require('../images/info.png')}
          />
          <TouchableOpacity>
            <Text style={styles.profileText}>Terms of use</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.Line} />
        <View style={{flexDirection: 'row'}}>
          <Image
            style={{width: 35, height: 35, marginTop: 20, marginHorizontal: 30}}
            source={require('../images/logout.png')}
          />
          <TouchableOpacity>
            <Text style={styles.profileText}>Logout</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.Line} />
        <Text style={styles.sub2text}>Version 1.1.1</Text>
      </ScrollView>
    </View>
  );
};

export default Search;

const styles = StyleSheet.create({
  image: {
    width: 380,
    height: 100,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    alignSelf: 'center',
    marginTop: 10,
    color: '#333333',
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '500',
    alignSelf: 'center',
    marginTop: 5,
    color: '#333333',
  },
  sub2text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '500',
    alignSelf: 'center',
    marginTop: 20,
    color: '#333333',
    marginBottom: 20,
  },
  profileText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    marginTop: 25,
    color: '#333333',
    marginLeft: -15,
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#dadce0aa',
    marginTop: 10,
    // marginHorizontal: 30,
    // marginLeft: 25,
  },
});
