import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderLight} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const Notification = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <HeaderLight onPress={() => navigation.goBack()} />
      <Text style={styles.text}>Notifications</Text>
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 40, height: 40, marginTop: 30, marginHorizontal: 30}}
          source={require('../images/notification.png')}
        />
        <View>
          <Text style={styles.profileText}>Homework Uploaded by teacher</Text>
          <Text style={styles.profilesubtext}>10 july 2019</Text>
        </View>
      </View>
      <View style={styles.Line} />
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 40, height: 40, marginTop: 30, marginHorizontal: 30}}
          source={require('../images/notification.png')}
        />
        <View>
          <Text style={styles.profileText}>New Quiz Uploaded by teacher</Text>
          <Text style={styles.profilesubtext}>10 july 2019</Text>
        </View>
      </View>
      <View style={styles.Line} />
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 40, height: 40, marginTop: 30, marginHorizontal: 30}}
          source={require('../images/notification.png')}
        />
        <View>
          <Text style={styles.profileText}>Homework Uploaded by teacher</Text>
          <Text style={styles.profilesubtext}>10 july 2019</Text>
        </View>
      </View>
      <View style={styles.Line} />
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 40, height: 40, marginTop: 30, marginHorizontal: 30}}
          source={require('../images/notification.png')}
        />
        <View>
          <Text style={styles.profileText}>New Quiz Uploaded by teacher</Text>
          <Text style={styles.profilesubtext}>10 july 2019</Text>
        </View>
      </View>
      <View style={styles.Line} />
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 40, height: 40, marginTop: 30, marginHorizontal: 30}}
          source={require('../images/notification.png')}
        />
        <View>
          <Text style={styles.profileText}>Homework Uploaded by teacher</Text>
          <Text style={styles.profilesubtext}>10 july 2019</Text>
        </View>
      </View>
      <View style={styles.Line} />
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 40, height: 40, marginTop: 30, marginHorizontal: 30}}
          source={require('../images/notification.png')}
        />
        <View>
          <Text style={styles.profileText}>New Quiz Uploaded by teacher</Text>
          <Text style={styles.profilesubtext}>10 july 2019</Text>
        </View>
      </View>
      <View style={styles.Line} />
    </View>
  );
};

export default Notification;

const styles = StyleSheet.create({
  image: {
    width: 50,
    height: 50,
    marginTop: 20,
    marginHorizontal: '25%',
  },
  subImage: {
    width: 101,
    height: 144,
    marginHorizontal: 30,
    marginTop: 20,
  },
  text: {
    marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
  },
  subtext: {
    marginTop: -10,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#8F92A1',
    lineHeight: 25,
  },
  box: {
    padding: 10,
    marginHorizontal: 30,
    backgroundColor: '#EFF2F4',
    borderRadius: 10,
    marginTop: 20,
    flexDirection: 'row',
  },
  boxtext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
    marginHorizontal: 10,
    marginTop: 5,
  },
  subBoxtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#333333',
    lineHeight: 22,
  },
  boximage: {
    width: 8,
    height: 13,
    marginTop: 15,
    marginHorizontal: '15%',
  },
  profileText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 30,
    color: '#121213',
    marginLeft: -10,
  },
  profilesubtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 12,
    fontWeight: '500',
    marginTop: 10,
    marginLeft: -10,
    color: '#333333',
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#dadce0aa',
    marginTop: 10,
    marginHorizontal: 30,
    // marginLeft: 25,
  },
});
