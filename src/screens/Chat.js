import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderLight} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const Chat = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <HeaderLight onPress={() => navigation.goBack()} />
      <Text style={styles.text}>My Chats</Text>
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 55, height: 55, marginTop: 30, marginHorizontal: 30}}
          source={require('../images/gamer.png')}
        />
        <Text style={styles.profileText}>Rahul Malhotra</Text>
        <Text style={styles.profilesubtext}>5:30 PM</Text>
      </View>
      <View style={styles.Line} />
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 55, height: 55, marginTop: 30, marginHorizontal: 30}}
          source={require('../images/gamer.png')}
        />
        <Text style={styles.profileText}>Rahul Malhotra</Text>
        <Text style={styles.profilesubtext}>5:30 PM</Text>
      </View>
      <View style={styles.Line} />
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 55, height: 55, marginTop: 30, marginHorizontal: 30}}
          source={require('../images/gamer.png')}
        />
        <Text style={styles.profileText}>Rahul Malhotra</Text>
        <Text style={styles.profilesubtext}>5:30 PM</Text>
      </View>
      <View style={styles.Line} />
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 55, height: 55, marginTop: 30, marginHorizontal: 30}}
          source={require('../images/gamer.png')}
        />
        <Text style={styles.profileText}>Rahul Malhotra</Text>
        <Text style={styles.profilesubtext}>5:30 PM</Text>
      </View>
      <View style={styles.Line} />
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 55, height: 55, marginTop: 30, marginHorizontal: 30}}
          source={require('../images/gamer.png')}
        />
        <Text style={styles.profileText}>Rahul Malhotra</Text>
        <Text style={styles.profilesubtext}>5:30 PM</Text>
      </View>
      <View style={styles.Line} />
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 55, height: 55, marginTop: 30, marginHorizontal: 30}}
          source={require('../images/gamer.png')}
        />
        <Text style={styles.profileText}>Rahul Malhotra</Text>
        <Text style={styles.profilesubtext}>5:30 PM</Text>
      </View>
      <View style={styles.Line} />
    </View>
  );
};

export default Chat;

const styles = StyleSheet.create({
  image: {
    width: 50,
    height: 50,
    marginTop: 20,
    marginHorizontal: '25%',
  },
  subImage: {
    width: 101,
    height: 144,
    marginHorizontal: 30,
    marginTop: 20,
  },
  text: {
    marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
  },
  subtext: {
    marginTop: -10,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#8F92A1',
    lineHeight: 25,
  },
  box: {
    padding: 10,
    marginHorizontal: 30,
    backgroundColor: '#EFF2F4',
    borderRadius: 10,
    marginTop: 20,
    flexDirection: 'row',
  },
  boxtext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
    marginHorizontal: 10,
    marginTop: 5,
  },
  subBoxtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#333333',
    lineHeight: 22,
  },
  boximage: {
    width: 8,
    height: 13,
    marginTop: 15,
    marginHorizontal: '15%',
  },
  profileText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 40,
    color: '#121213',
    marginLeft: -10,
  },
  profilesubtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 12,
    fontWeight: '500',
    marginTop: 40,
    marginLeft: 'auto',
    marginHorizontal: 30,
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#dadce0aa',
    marginTop: 10,
    marginHorizontal: 30,
    // marginLeft: 25,
  },
});
