import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderLight} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const Discussion = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <HeaderLight onPress={() => navigation.goBack()} />
      <Text style={styles.text}>Select Discussions Type</Text>
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 60, height: 60, marginTop: 30, marginHorizontal: 30}}
          source={require('../images/chat-group.png')}
        />
        <TouchableOpacity
          onPress={() => navigation.navigate('DiscussionForum')}>
          <Text style={styles.profileText}>Discussion Forum</Text>
        </TouchableOpacity>

        <Image
          style={styles.arrow}
          source={require('../images/arrow-back.png')}
        />
      </View>
      <View style={styles.Line} />
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 60, height: 60, marginTop: 30, marginHorizontal: 30}}
          source={require('../images/speak.png')}
        />
        <TouchableOpacity onPress={() => navigation.navigate('Chat')}>
          <Text style={styles.profileText}>Chat Room</Text>
        </TouchableOpacity>

        <Image
          style={styles.arrow}
          source={require('../images/arrow-back.png')}
        />
      </View>
      <View style={styles.Line} />
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 60, height: 60, marginTop: 30, marginHorizontal: 30}}
          source={require('../images/notification1.png')}
        />
        <TouchableOpacity onPress={() => navigation.navigate('Notification')}>
          <Text style={styles.profileText}>Notification</Text>
        </TouchableOpacity>

        <Image
          style={styles.arrow}
          source={require('../images/arrow-back.png')}
        />
      </View>
    </View>
  );
};

export default Discussion;

const styles = StyleSheet.create({
  text: {
    marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
  },
  profileText: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 40,
    color: '#333333',
    marginLeft: -10,
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#dadce0aa',
    marginTop: 10,
    marginHorizontal: 100,
    marginRight: 30,
  },
  arrow: {
    width: 10,
    height: 17,
    marginTop: 45,
    marginLeft: 'auto',
    marginHorizontal: 30,
  },
});
