import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  Button,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import Slider from 'react-native-slider';
import Swiper from 'react-native-deck-swiper';
import {TouchableRipple} from 'react-native-paper';
const {height} = Dimensions.get('window');

const Flash = ({navigation}) => {
  const [range, setRange] = useState(6);
  const [sliding, setSliding] = useState('Inactive');

  const flashcheckout = () => {
    setTimeout(() => {
      navigation.navigate('Study2');
    }, 300);
  };
  return (
    <View style={{backgroundColor: '#EFF2F4', flex: 1}}>
      <StatusBarDark />
      <Swiper
        cards={[
          'Define Non-for-Profit Organisation.',
          'Define Non-for-Profit Organisation.',
          'Define Non-for-Profit Organisation.',
          'Define Non-for-Profit Organisation.',
          'Define Non-for-Profit Organisation.',
          'Define Non-for-Profit Organisation.',
          'Define Non-for-Profit Organisation.',
        ]}
        renderCard={card => {
          <View style={{flexDirection: 'row'}}>
            <TouchableOpacity onPress={() => navigation.navigate('Study2')}>
              <Image
                style={styles.image}
                source={require('../images/close.png')}
              />
            </TouchableOpacity>
            <Text style={styles.text}>1/{range}</Text>
          </View>;
          return (
            <View style={styles.container}>
              <Text style={styles.texted}>{card}</Text>
              <TouchableOpacity
                style={styles.touch2}
                onPress={() => navigation.navigate('Study2')}>
                <Text style={styles.touch2text}>Tab to flip card</Text>
              </TouchableOpacity>
            </View>
          );
        }}
        onSwiped={cardIndex => {
          console.log(cardIndex);
        }}
        onSwipedAll={() => {
          console.log('onSwipedAll');
        }}
        cardIndex={0}
        backgroundColor={'#EFF2F4'}
        stackSize={3}>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity>
            <Image
              style={styles.image}
              source={require('../images/close.png')}
            />
          </TouchableOpacity>
          <Text style={styles.text}>1/{range}</Text>
        </View>
        <ScrollView decelerationRate={0.5}>
          <Slider
            minimumValue={0}
            maximumValue={1}
            minimumTrackTintColor="#FA6400"
            maximumTrackTintColor="#fff"
            thumbTintColor="#FA6400"
            value={0}
            onValueChange={value => setRange(parseInt(value * 6))}
            style={{width: 303, height: 100, alignSelf: 'center'}}
          />
          <TouchableRipple
            style={styles.touch}
            onPress={() => flashcheckout()}
            rippleColor="rgba(0, 0, 0, .32)">
            <Text style={styles.touchtext}>Continue</Text>
          </TouchableRipple>
        </ScrollView>
      </Swiper>
    </View>
  );
};

export default Flash;

const styles = StyleSheet.create({
  image: {
    // marginTop: height / 3,
    width: 15,
    height: 15,
    resizeMode: 'contain',
    marginTop: 40,
    marginHorizontal: 30,
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
    marginHorizontal: '28%',
    marginTop: 32,
  },
  container: {
    height: 485,
    width: 315,
    borderRadius: 15,
    borderWidth: 2,
    borderColor: '#E8E8E8',
    backgroundColor: 'white',
    alignSelf: 'center',
    marginTop: '30%',
  },
  texted: {
    textAlign: 'center',
    marginTop: '60%',
    fontSize: 25,
    backgroundColor: 'transparent',
  },
  touch: {
    padding: 10,
    marginHorizontal: 30,
    borderRadius: 5,
    backgroundColor: '#0253B3',
    marginTop: '140%',
    elevation: 15,
    marginBottom: 20,
  },
  touchtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 22,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
  },
  touch2: {
    padding: 10,
    marginHorizontal: -2,
    backgroundColor: '#0253B3',
    marginTop: '62%',
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
  },
  touch2text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
  },
});
