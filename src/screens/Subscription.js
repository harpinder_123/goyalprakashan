import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
  TextInput,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderImageDark} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const Subscription = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#023047', flex: 1}}>
      <StatusBarDark />
      <HeaderImageDark
        onPress={() => navigation.goBack()}
        title="Class 12 Subscription"
      />
      <View
        style={{
          width: 250,
          height: 30,
          borderWidth: 1,
          borderColor: '#ffffffaa',
          borderRadius: 5,
          alignSelf: 'center',
          marginTop: 40,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <View
            style={{
              width: 86,
              height: 30,
              backgroundColor: '#fff',
              borderRadius: 5,
            }}>
            <Text style={styles.text}>Basic</Text>
          </View>

          <Text style={styles.subtext}>Standard</Text>
          <Text style={styles.sub2text}>Premium</Text>
        </View>
      </View>
      <View style={{alignSelf: 'center'}}>
        <View style={{flexDirection: 'row', marginTop: 10}}>
          <Image
            style={{width: 20, height: 20, marginTop: 20, marginHorizontal: 30}}
            source={require('../images/check.png')}
          />
          <Text style={styles.sub3text}>Online Test</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Image
            style={{width: 20, height: 20, marginTop: 20, marginHorizontal: 30}}
            source={require('../images/check.png')}
          />
          <Text style={styles.sub3text}>Video Lectures</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Image
            style={{width: 20, height: 20, marginTop: 20, marginHorizontal: 30}}
            source={require('../images/check.png')}
          />
          <Text style={styles.sub3text}>Worksheet</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Image
            style={{width: 20, height: 20, marginTop: 20, marginHorizontal: 30}}
            source={require('../images/check.png')}
          />
          <Text style={styles.sub3text}>Assignments</Text>
        </View>
      </View>
      <TouchableOpacity
        style={styles.container}
        onPress={() => navigation.navigate('ActivePlan')}>
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.containerText}>12 Months</Text>
          <View style={{marginLeft: 'auto'}}>
            <Text style={styles.container2Text}>₹ 2,100/mo</Text>
            <Text style={styles.container3Text}>₹ 25,000</Text>
          </View>
          <Image
            style={{width: 7, height: 13, marginTop: 20, marginHorizontal: 15}}
            source={require('../images/arrow-back.png')}
          />
        </View>
      </TouchableOpacity>
      <TouchableOpacity style={styles.container}>
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.containerText}>9 Months</Text>
          <View style={{marginLeft: 'auto'}}>
            <Text style={styles.container2Text}>₹ 2,100/mo</Text>
            <Text style={styles.container3Text}>₹ 25,000</Text>
          </View>
          <Image
            style={{width: 7, height: 13, marginTop: 20, marginHorizontal: 15}}
            source={require('../images/arrow-back.png')}
          />
        </View>
      </TouchableOpacity>
      <TouchableOpacity style={styles.container}>
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.containerText}>3 Months</Text>
          <View style={{marginLeft: 'auto'}}>
            <Text style={styles.container2Text}>₹ 2,100/mo</Text>
            <Text style={styles.container3Text}>₹ 25,000</Text>
          </View>
          <Image
            style={{width: 7, height: 13, marginTop: 20, marginHorizontal: 15}}
            source={require('../images/arrow-back.png')}
          />
        </View>
      </TouchableOpacity>
      <Text style={styles.endtext}>To be paid as a one-time payment</Text>
    </View>
  );
};

export default Subscription;

const styles = StyleSheet.create({
  container: {
    padding: 10,
    marginHorizontal: 30,
    backgroundColor: '#fff',
    borderRadius: 5,
    marginTop: 20,
  },
  text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#023047',
    textAlign: 'center',
    marginTop: 5,
  },
  containerText: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 15,
  },
  container2Text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 5,
  },
  container3Text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: 'gray',
    marginTop: 5,
    marginLeft: 25,
    // marginHorizontal: 15,
  },
  endtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#ffffffaa',
    marginTop: 20,
    textAlign: 'center',
  },
  end2image: {
    width: 16,
    height: 22,
    marginHorizontal: 30,
    marginTop: 20,
    marginLeft: 'auto',
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#fff',
    marginTop: 5,
  },
  sub2text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#fff',
    marginTop: 5,
    marginHorizontal: 10,
  },
  sub3text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#ffffff',
    marginTop: 17,
    marginLeft: -15,
  },
  box: {
    padding: 15,
    marginHorizontal: 30,
    backgroundColor: '#ffffff',
    borderRadius: 5,
    elevation: 3,
    marginTop: 20,
  },
  box2: {
    padding: 15,
    marginHorizontal: 30,
    backgroundColor: '#ffffff',
    borderRadius: 5,
    // elevation: 3,
    marginTop: 20,
    borderColor: '#6DD400',
    borderWidth: 2,
    backgroundColor: '#edf9e1',
  },
  box3: {
    padding: 15,
    marginHorizontal: 30,
    backgroundColor: '#ffffff',
    borderRadius: 5,
    // elevation: 3,
    marginTop: 20,
    borderColor: '#E02020',
    borderWidth: 2,
    backgroundColor: '#f5dddd',
  },
  boxtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#333333',
    marginHorizontal: 10,
  },
  box2text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#6DD400',
    marginHorizontal: 10,
  },
  box3text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#E02020',
    marginHorizontal: 10,
  },
  box4text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#333333',
    marginHorizontal: 10,
    marginTop: 10,
    lineHeight: 25,
  },
  touch2: {
    width: 160,
    height: 50,
    borderRadius: 5,
    backgroundColor: '#6D7278',
    marginTop: '10%',
    elevation: 15,
  },
  touch2text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 17,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 12,
  },
  touch3: {
    width: 160,
    height: 50,
    borderRadius: 5,
    backgroundColor: '#0253B3',
    marginTop: '10%',
    elevation: 15,
    marginBottom: 20,
  },
  touch3text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 17,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 12,
  },
});
