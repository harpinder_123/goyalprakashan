import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderLight} from '../Custom/CustomView';
import {TouchableRipple} from 'react-native-paper';
const {height} = Dimensions.get('window');

const TestDetails = ({navigation}) => {
  const testcheckout = () => {
    setTimeout(() => {
      navigation.navigate('TabNavigator');
    }, 300);
  };
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <HeaderLight onPress={() => navigation.goBack()} />
      <ScrollView decelerationRate={0.5}>
        <Text style={styles.text}>Accountancy Online Mock Test - XII</Text>
        <View style={styles.box}>
          <Text style={styles.boxtext}>FREE</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Image
            style={styles.image}
            source={require('../images/calendar.png')}
          />
          <View>
            <Text style={styles.subtext}>Started at 9:00 AM</Text>
            <Text style={styles.sub2text}>Aug 6</Text>
          </View>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Image style={styles.image} source={require('../images/play.png')} />
          <Text style={styles.sub3text}>5 Lessons</Text>
        </View>
        <Text style={styles.maintext}>Details</Text>
        <Text style={styles.main2text}>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and
          scrambled it to make a type specimen book.
        </Text>
        <TouchableRipple
          style={styles.touch}
          onPress={() => testcheckout()}
          rippleColor="rgba(0, 0, 0, .32)">
          <Text style={styles.touchtext}>Enroll</Text>
        </TouchableRipple>
      </ScrollView>
    </View>
  );
};

export default TestDetails;

const styles = StyleSheet.create({
  text: {
    marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
    lineHeight: 40,
  },
  box: {
    width: 47,
    height: 25,
    backgroundColor: '#FA6400',
    borderRadius: 5,
    marginHorizontal: 145,
    marginTop: -32,
  },
  boxtext: {
    color: '#fff',
    alignSelf: 'center',
    fontSize: 12,
    marginTop: 4,
  },
  image: {
    width: 40,
    height: 40,
    marginTop: 30,
    marginHorizontal: 30,
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 30,
    marginLeft: -10,
  },
  sub2text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#333333',
    // marginTop: 5,
    marginLeft: -10,
  },
  sub3text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 35,
    marginLeft: -10,
  },
  maintext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 30,
    marginHorizontal: 30,
  },
  main2text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: 'grey',
    marginTop: 10,
    marginHorizontal: 30,
    lineHeight: 30,
    textAlign: 'justify',
  },
  touch: {
    padding: 10,
    marginHorizontal: 30,
    borderRadius: 5,
    backgroundColor: '#0253B3',
    marginTop: '40%',
    elevation: 5,
    marginBottom: 20,
  },
  touchtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 22,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
  },
});
