import React, {useState} from 'react';
import {
  Dimensions,
  Image,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';

const {height, width} = Dimensions.get('window');
const PasswordUpdate = ({navigation}) => {
  const [modalOpen, setModalOpen] = useState(true);

  return (
    <Modal visible={modalOpen} transparent={true}>
      <View style={styles.modal_View}>
        <View style={styles.mdtop}>
          <Image source={require('../images/tick.png')} style={styles.image} />

          <Text style={styles.text}>
            Your Online Support {`\n`}Activated Successfully.
          </Text>
          <TouchableOpacity
            style={styles.touch}
            onPress={() => navigation.navigate('BookDetails')}>
            <Text style={styles.touchtext}>VIEW BOOK</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default PasswordUpdate;

const styles = StyleSheet.create({
  modal_View: {
    backgroundColor: '#000000aa',
    flex: 1,
  },
  mdtop: {
    backgroundColor: '#FFFFFF',
    marginTop: height / 3,
    marginHorizontal: 20,
    borderRadius: 20,
  },
  image: {
    width: 80,
    height: 80,
    alignSelf: 'center',
    marginTop: 20,
  },
  text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#1E2432',
    textAlign: 'center',
    marginTop: 10,
    lineHeight: 35,
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#838383',
    textAlign: 'center',
    marginTop: 20,
    marginBottom: 20,
  },
  touch: {
    width: 135,
    height: 40,
    borderRadius: 25,
    backgroundColor: '#0253B3',
    marginTop: 20,
    marginBottom: 20,
    alignSelf: 'center',
  },
  touchtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 8,
  },
});
