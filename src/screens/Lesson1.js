import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {Header2} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const Lesson1 = ({navigation}) => {
  return (
    <View style={styles.Container}>
      <StatusBarDark />
      <Header2
        onPress={() => navigation.goBack()}
        title="Receipt and Payment Account"
      />
    </View>
  );
};

export default Lesson1;

const styles = StyleSheet.create({
  Container: {
    backgroundColor: '#FFFFFF',
    flex: 1,
  },
});
