import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {TextField} from 'react-native-material-textfield';
import {Header} from '../Custom/CustomView';
const {height} = Dimensions.get('window');
export const CustomInputField = props => (
  <>
    <View style={styles.middleView}>
      <TextField
        containerStyle={styles.tfStyle}
        disableUnderline={true}
        tintColor="#8D92A3"
        {...props}
      />
    </View>
  </>
);

const ForgotPassword = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} />
      <Text style={styles.text}>Forgot Password?</Text>
      <Text style={styles.subtext}>
        Enter your register Email/Mobile number to reset your password.
      </Text>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          marginHorizontal: 35,
        }}>
        <View>
          <Text style={styles.label}>Code</Text>
          <Text style={styles.sublabel}>+91</Text>
          <View style={styles.Line} />
        </View>

        <CustomInputField
          label="Mobile"
          //   defaultValue={state.emailphone}
          useNativeDriver={true}
          keyboardType={'number-pad'}
          maxLength={10}
        />
      </View>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'center',
        }}>
        <View style={styles.subLine} />
        <Text style={styles.continue}>OR</Text>
        <View style={styles.subLine} />
      </View>
      <CustomInputField
        label="Email"
        //   defaultValue={state.password}
        useNativeDriver={true}
      />
      <TouchableOpacity
        style={styles.touch}
        onPress={() => navigation.navigate('Otp')}>
        <Text style={styles.touchtext}>Verify</Text>
      </TouchableOpacity>
    </View>
  );
};

export default ForgotPassword;

const styles = StyleSheet.create({
  text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 30,
    fontWeight: 'bold',
    marginTop: 10,
    color: '#1E1F20',
    marginHorizontal: 30,
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#8F92A1',
    marginHorizontal: 30,
    lineHeight: 25,
    marginTop: 5,
  },
  label: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#8F92A1',
    marginHorizontal: 30,
    marginTop: 30,
  },
  sublabel: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#000000',
    marginHorizontal: 30,
  },
  middleView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  tfStyle: {
    width: '85%',
    marginHorizontal: 30,
    marginTop: 17,
  },
  Line: {
    height: 1,
    width: Dimensions.get('window').width / 8,
    borderRadius: 5,
    backgroundColor: '#979797',
    marginTop: 9,
    marginLeft: 25,
  },
  subLine: {
    height: 1,
    width: Dimensions.get('window').width / 15,
    borderRadius: 5,
    backgroundColor: '#979797',
    marginTop: 40,
    marginLeft: 15,
  },
  continue: {
    fontFamily: 'nunito',
    fontSize: 15,
    fontWeight: '700',
    color: '#8F92A1',
    marginTop: 35,
    marginLeft: 10,
  },
  touch: {
    padding: 10,
    marginHorizontal: 30,
    borderRadius: 5,
    backgroundColor: '#0253B3',
    marginTop: 40,
    elevation: 15,
  },
  touchtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 22,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
  },
});
