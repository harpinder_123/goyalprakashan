import React, {useState} from 'react';
import {
  Dimensions,
  Image,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {Dropdown} from 'react-native-material-dropdown-v2-fixed';
const {height, width} = Dimensions.get('window');
let data = [
  {
    value: 'Maths',
  },
  {
    value: 'Science',
  },
];
const Community = ({navigation}) => {
  const [modalOpen, setModalOpen] = useState(true);

  return (
    <Modal visible={modalOpen} transparent={true}>
      <View style={styles.modal_View}>
        <View style={styles.mdtop}>
          <TouchableOpacity onPress={() => navigation.navigate('TabNavigator')}>
            <Image
              style={{
                width: 15,
                height: 15,
                marginLeft: 'auto',
                marginHorizontal: 15,
                marginTop: 5,
              }}
              source={require('../images/close.png')}
            />
          </TouchableOpacity>
          <Image
            style={{width: 80, height: 80, alignSelf: 'center', marginTop: 10}}
            source={require('../images/science.png')}
          />
          <Text style={styles.text}>Select Subject</Text>
          <Dropdown
            style={styles.drops}
            label="Subject"
            icon="cheveron-down"
            iconColor="#000000"
            icon={require('../images/drop.png')}
            data={data}
          />
          <TouchableOpacity
            style={styles.touch}
            onPress={() => navigation.navigate('Discussion')}>
            <Text style={styles.touchtext}>NEXT</Text>
          </TouchableOpacity>
        </View>
      </View>
    </Modal>
  );
};

export default Community;

const styles = StyleSheet.create({
  modal_View: {
    backgroundColor: '#000000aa',
    flex: 1,
  },
  mdtop: {
    backgroundColor: '#FFFFFF',
    marginTop: height / 3,
    marginHorizontal: 20,
    borderRadius: 20,
  },
  image: {
    width: 80,
    height: 80,
    alignSelf: 'center',
    marginTop: 20,
  },
  text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#1E2432',
    textAlign: 'center',
    marginTop: 5,
    lineHeight: 30,
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#838383',
    textAlign: 'center',
    marginTop: 20,
    marginBottom: 20,
  },
  drops: {
    backgroundColor: '#fff',
    marginHorizontal: 30,
    marginTop: 10,
  },
  touch: {
    width: 135,
    height: 40,
    borderRadius: 25,
    backgroundColor: '#0253B3',
    marginTop: 20,
    marginBottom: 20,
    alignSelf: 'center',
  },
  touchtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 8,
  },
});
