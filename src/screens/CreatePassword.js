import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {Header} from '../Custom/CustomView';
import {TextField} from 'react-native-material-textfield';
const {height} = Dimensions.get('window');
export const CustomInputField = props => (
  <>
    <TextField
      containerStyle={styles.tfStyle}
      disableUnderline={true}
      tintColor="#8D92A3"
      {...props}
    />
  </>
);

const CreatePassword = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} />
      <Text style={styles.text}>Create Password</Text>
      <Text style={styles.subtext}>Please enter your new password</Text>
      <View style={{marginTop: 60}}>
        <CustomInputField
          label="New Password"
          //   defaultValue={state.emailphone}
          useNativeDriver={true}
          secureTextEntry={true}
        />
        <CustomInputField
          label="Confirm Password"
          //   defaultValue={state.password}
          useNativeDriver={true}
          secureTextEntry={true}
        />
      </View>
      <TouchableOpacity
        style={styles.touch}
        onPress={() => navigation.navigate('PasswordUpdate')}>
        <Text style={styles.touchtext}>Submit</Text>
      </TouchableOpacity>
    </View>
  );
};

export default CreatePassword;

const styles = StyleSheet.create({
  text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 30,
    fontWeight: 'bold',
    marginTop: 10,
    color: '#1E1F20',
    marginHorizontal: 30,
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#8F92A1',
    marginHorizontal: 30,
    lineHeight: 25,
    marginTop: 5,
  },
  middleView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  tfStyle: {
    width: '85%',
    marginHorizontal: 30,
  },
  touch: {
    padding: 10,
    marginHorizontal: 30,
    borderRadius: 5,
    backgroundColor: '#0253B3',
    marginTop: 40,
    elevation: 15,
  },
  touchtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 22,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
  },
});
