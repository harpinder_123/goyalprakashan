import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderLight} from '../Custom/CustomView';
import {useNavigation} from '@react-navigation/native';
const {height} = Dimensions.get('window');

const Chapter = () => {
  const navigation = useNavigation();
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <HeaderLight onPress={() => navigation.goBack()} />
      <ScrollView decelerationRate={0.5}>
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.text}>Accountancy</Text>
          <Image
            style={styles.image}
            source={require('../images/account.png')}
          />
        </View>
        <Text style={styles.subtext}>11 Chapters</Text>
        <TouchableOpacity onPress={() => navigation.navigate('Study2')}>
          <View style={styles.box}>
            <Text style={styles.boxtext}>01</Text>
            <Text style={styles.subBoxtext}>
              Accounting for Not-for-Profit{`\n`}Organisations
            </Text>
            <Image
              style={styles.boximage}
              source={require('../images/arrow-back.png')}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.box}>
            <Text style={styles.boxtext}>02</Text>
            <Text style={styles.subBoxtext}>
              Accounting for Not-for-Profit{`\n`}Organisations
            </Text>
            <Image
              style={styles.boximage}
              source={require('../images/arrow-back.png')}
            />
          </View>
        </TouchableOpacity>
        <TouchableOpacity>
          <View style={styles.box}>
            <Text style={styles.boxtext}>03</Text>
            <Text style={styles.subBoxtext}>
              Accounting for Not-for-Profit{`\n`}Organisations
            </Text>
            <Image
              style={styles.boximage}
              source={require('../images/arrow-back.png')}
            />
          </View>
        </TouchableOpacity>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={styles.texted}>Flash Cards</Text>
          <Text style={styles.endText}>View all</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <ScrollView horizontal decelerationRate={0.5}>
            <TouchableOpacity onPress={() => navigation.navigate('Flash')}>
              <View style={styles.box3}>
                <Image
                  style={{
                    width: 50,
                    height: 50,
                    alignSelf: 'center',
                    marginTop: 25,
                  }}
                  source={require('../images/flash.png')}
                />
              </View>
              <Text style={styles.box2text}>
                Income & Expenditure{`\n`}Account
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{marginLeft: -40}}>
              <View style={styles.box3}>
                <Image
                  style={{
                    width: 50,
                    height: 50,
                    alignSelf: 'center',
                    marginTop: 25,
                  }}
                  source={require('../images/flash.png')}
                />
              </View>
              <Text style={styles.box2text}>
                Income & Expenditure{`\n`}Account
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{marginLeft: -40}}>
              <View style={styles.box3}>
                <Image
                  style={{
                    width: 50,
                    height: 50,
                    alignSelf: 'center',
                    marginTop: 25,
                  }}
                  source={require('../images/flash.png')}
                />
              </View>
              <Text style={styles.box2text}>
                Income & Expenditure{`\n`}Account
              </Text>
            </TouchableOpacity>
          </ScrollView>
        </View>
        <Text style={styles.texted}>Online Test</Text>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            onPress={() => navigation.navigate('TestInstructions')}>
            <View style={styles.box4}>
              <Image
                style={{
                  width: 50,
                  height: 50,
                  alignSelf: 'center',
                  marginTop: 25,
                }}
                source={require('../images/test2.png')}
              />
            </View>
          </TouchableOpacity>
          <TouchableOpacity style={{marginLeft: -40}}>
            <View style={styles.box4}>
              <Image
                style={{
                  width: 50,
                  height: 50,
                  alignSelf: 'center',
                  marginTop: 25,
                }}
                source={require('../images/test2.png')}
              />
            </View>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default Chapter;

const styles = StyleSheet.create({
  image: {
    width: 50,
    height: 50,
    marginTop: 20,
    marginHorizontal: '25%',
  },
  subImage: {
    width: 101,
    height: 144,
    marginHorizontal: 30,
    marginTop: 20,
  },
  text: {
    marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
  },
  subtext: {
    marginTop: -10,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#8F92A1',
    lineHeight: 25,
  },
  box: {
    padding: 10,
    marginHorizontal: 30,
    backgroundColor: '#EFF2F4',
    borderRadius: 10,
    marginTop: 20,
    flexDirection: 'row',
  },
  box3: {
    width: 140,
    height: 100,
    marginHorizontal: 30,
    backgroundColor: '#E5E1E5',
    borderRadius: 15,
    marginTop: 20,
  },
  box4: {
    width: 140,
    height: 100,
    marginHorizontal: 30,
    backgroundColor: '#FDBF43',
    borderRadius: 15,
    marginTop: 20,
    marginBottom: 20,
  },
  boxtext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
    marginHorizontal: 10,
    marginTop: 5,
  },
  box2text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#333333',
    marginHorizontal: 35,
    marginTop: 10,
  },
  subBoxtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#333333',
    lineHeight: 22,
  },
  boximage: {
    width: 8,
    height: 13,
    marginTop: 15,
    marginHorizontal: '15%',
  },
  texted: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 20,
    color: '#1E1F20',
    marginHorizontal: 30,
  },
  endText: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    marginTop: 20,
    color: '#FA6400',
    marginHorizontal: 20,
  },
});
