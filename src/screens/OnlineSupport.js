import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {TextField} from 'react-native-material-textfield';
import {HeaderLight} from '../Custom/CustomView';
const {height} = Dimensions.get('window');
export const CustomInputField = props => (
  <>
    <View style={styles.middleView}>
      <TextField
        containerStyle={styles.tfStyle}
        disableUnderline={true}
        tintColor="#8D92A3"
        {...props}
      />
    </View>
  </>
);

const OnlineSupport = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <HeaderLight onPress={() => navigation.goBack()} />
      <ScrollView decelerationRate={0.5}>
        <Text style={styles.text}>Online Support</Text>
        <Text style={styles.subtext}>
          Please enter voucher code below to activate online support
        </Text>
        <CustomInputField
          label="Enter Voucher Code"
          //   defaultValue={state.emailphone}
          useNativeDriver={true}
        />
        <Text style={styles.noteText}>
          <Text style={{color: '#E02020'}}>Note:-</Text> Voucher code is
          available in the books supplied by Goyal Brother Prakashan
        </Text>
        <View
          style={{
            flexDirection: 'row',
            alignSelf: 'center',
          }}>
          <View style={styles.subLine} />
          <Text style={styles.continue}>OR</Text>
          <View style={styles.subLine} />
        </View>
        <TouchableOpacity onPress={() => navigation.navigate('ScanCode')}>
          <Image style={styles.image} source={require('../images/scan.png')} />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.touch}
          onPress={() => navigation.navigate('OnlinePopup')}>
          <Text style={styles.touchtext}>Activate</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default OnlineSupport;

const styles = StyleSheet.create({
  image: {
    width: 210,
    height: 120,
    alignSelf: 'center',
    marginTop: 20,
  },
  subImage: {
    width: 101,
    height: 144,
    marginHorizontal: 30,
    marginTop: 20,
  },
  text: {
    marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
  },
  subtext: {
    marginTop: 10,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#8F92A1',
    lineHeight: 25,
  },
  middleView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  tfStyle: {
    width: '85%',
    marginHorizontal: 30,
    marginTop: 20,
  },
  noteText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 11,
    fontWeight: '500',
    color: '#333333',
    marginTop: 10,
    marginHorizontal: 30,
    lineHeight: 20,
  },
  subLine: {
    height: 1,
    width: Dimensions.get('window').width / 25,
    borderRadius: 5,
    backgroundColor: '#979797',
    marginTop: 40,
    marginLeft: 5,
  },
  continue: {
    fontFamily: 'nunito',
    fontSize: 15,
    fontWeight: '700',
    color: '#8F92A1',
    marginTop: 28,
    marginLeft: 5,
  },
  touch: {
    padding: 10,
    marginHorizontal: 30,
    borderRadius: 5,
    backgroundColor: '#0253B3',
    marginTop: '40%',
    elevation: 15,
    marginBottom: 10,
  },
  touchtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 22,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
  },
});
