import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderLight} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const Article = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <HeaderLight onPress={() => navigation.goBack()} />
      <TouchableOpacity>
        <Image
          style={styles.subimage}
          source={require('../images/share.png')}
        />
      </TouchableOpacity>
      <ScrollView decelerationRate={0.5}>
        <Text style={styles.text}>Updates</Text>
        <Text style={styles.profilesubtext}>27 Sep 2021</Text>
        <Text style={styles.profileText}>
          Which Asian countries are US students studying abroad at?
        </Text>
        <ImageBackground
          style={styles.image}
          source={require('../images/Rectangle1.png')}>
          <TouchableOpacity>
            <Image
              style={{
                width: 60,
                height: 60,
                alignSelf: 'center',
                marginTop: 65,
              }}
              source={require('../images/Play1.png')}
            />
          </TouchableOpacity>
        </ImageBackground>
        <Text style={styles.subtext}>
          Lorem Ipsum is simply dummy text of the printing and typesetting
          industry. Lorem Ipsum has been the industry's standard dummy text ever
          since the 1500s, when an unknown printer took a galley of type and
          scrambled it to make a type specimen book. It has survived not only
          five centuries, but also the leap into electronic typesetting,
          remaining essentially unchanged. It was popularised in the 1960s with
          the release of Letraset sheets containing Lorem Ipsum passages, and
          more recently with desktop publishing software like Aldus PageMaker
          including versions of Lorem Ipsum.
        </Text>
      </ScrollView>
    </View>
  );
};

export default Article;

const styles = StyleSheet.create({
  image: {
    width: 340,
    height: 200,
    resizeMode: 'contain',
    alignSelf: 'center',
    marginTop: 15,
  },
  subimage: {
    width: 35,
    height: 35,
    marginLeft: 'auto',
    marginTop: -50,
    marginHorizontal: 20,
  },
  text: {
    marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    marginTop: 10,
    color: '#333333',
    lineHeight: 30,
    textAlign: 'justify',
    marginHorizontal: 30,
  },
  profileText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 20,
    fontWeight: '500',
    marginTop: 10,
    color: '#121213',
    marginHorizontal: 30,
    lineHeight: 28,
  },
  profilesubtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 13,
    fontWeight: '500',
    marginTop: 20,
    color: '#FA6400',
    marginHorizontal: 30,
  },
  profile2subtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 13,
    fontWeight: '500',
    marginTop: 40,
    color: '#FA6400',
    marginHorizontal: 30,
  },
  profile3Text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 15,
    fontWeight: '500',
    marginTop: 10,
    color: '#8F92A1',
    marginHorizontal: 30,
  },
  endimage: {
    width: 120,
    height: 120,
    alignSelf: 'flex-end',
    marginHorizontal: 20,
    marginTop: -120,
  },
});
