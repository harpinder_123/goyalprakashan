import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
const {height} = Dimensions.get('window');
import {SafeAreaProvider} from 'react-native-safe-area-context';
const FlashCards = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#fff', flex: 1}}>
      <StatusBarDark />
      <SafeAreaProvider>
        <ImageBackground
          style={styles.image}
          source={require('../images/Flash-cards.png')}></ImageBackground>
      </SafeAreaProvider>
    </View>
  );
};

export default FlashCards;

const styles = StyleSheet.create({
  image: {
    marginTop: 10,
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    // alignSelf: 'center',
    // marginBottom: 40,
  },
});
