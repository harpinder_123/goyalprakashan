import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {Dropdown} from 'react-native-material-dropdown-v2-fixed';
import {TouchableRipple} from 'react-native-paper';
import {Header} from '../Custom/CustomView';
const {height} = Dimensions.get('window');
let drop = [
  {
    value: 'New Delhi',
  },
  {
    value: 'DPS',
  },
];

const CreatePassword = ({navigation}) => {
  const [selectedIndex2, setSelectedIndex2] = useState(0);
  const [data, setData] = useState([
    {name: 'CBSE', id: '1'},
    {name: 'ICSE', id: '2'},
    {name: 'Punjab Board', id: '3'},
    {name: 'Up Board', id: '4'},
    {name: 'Bihar Board', id: '5'},
  ]);
  const [selected, setSelected] = useState(0);
  const [col, setCol] = useState(0);
  const [num, setNum] = useState([
    {name: '12', id: '1'},
    {name: '11', id: '2'},
    {name: '10', id: '3'},
    {name: '9', id: '4'},
    {name: '8', id: '5'},
    {name: '8', id: '6'},
    {name: '8', id: '7'},
    {name: '8', id: '8'},
  ]);
  const [selectedIndex1, setSelectedIndex1] = useState(0);
  const [state, setState] = useState([
    {name: 'Science (PCM)', id: '1'},
    {name: 'Science (PCB)', id: '2'},
    {name: 'Commerce', id: '3'},
  ]);

  const profilecheckout = () => {
    setTimeout(() => {
      navigation.navigate('ProfileCreated');
    }, 300);
  };

  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} />
      <ScrollView decelerationRate={0.5}>
        <Text style={styles.text}>Create your profile</Text>
        <Text style={styles.subtext}>Select Board</Text>
        <FlatList
          numColumns={3}
          keyExtractor={item => item.id}
          data={data}
          renderItem={({item, index}) => (
            <TouchableOpacity
              onPress={() => setSelectedIndex1(index)}
              style={{
                borderWidth: 1,
                borderColor: '#EFF2F4',
                backgroundColor:
                  index == selectedIndex1 ? '#FA6400' : '#EFF2F4',
                borderRadius: 16,
                padding: 10,
                marginTop: 15,
                marginLeft: 30,
              }}>
              <Text
                style={{
                  color: index == selectedIndex1 ? 'white' : 'black',
                  fontFamily: 'AvenirLTStd-Medium',
                  fontSize: 13,
                  fontWeight: '500',
                  textAlign: 'center',
                }}>
                {item.name}
              </Text>
            </TouchableOpacity>
          )}
        />
        <Text style={styles.subtext}>Select Class</Text>
        <FlatList
          numColumns={4}
          keyExtractor={item => item.id}
          data={num}
          renderItem={({item, index}) => (
            <TouchableOpacity
              onPress={() => setSelected(index)}
              style={{
                borderWidth: 1,
                borderColor: '#EFF2F4',
                backgroundColor: index == selected ? '#FA6400' : '#EFF2F4',
                borderRadius: 16,
                padding: 10,
                marginTop: 15,
                marginLeft: 20,
                width: 60,
              }}>
              <Text
                style={{
                  color: index == selected ? '#ffffff' : '#333333',
                  fontFamily: 'AvenirLTStd-Medium',
                  fontSize: 13,
                  fontWeight: '500',
                  textAlign: 'center',
                }}>
                {item.name}
              </Text>
            </TouchableOpacity>
          )}
        />
        <Text style={styles.subtext}>Select Stream</Text>
        <FlatList
          numColumns={2}
          keyExtractor={item => item.id}
          data={state}
          renderItem={({item, index}) => (
            <TouchableOpacity
              onPress={() => setSelectedIndex2(index)}
              style={{
                borderWidth: 1,
                borderColor: '#EFF2F4',
                backgroundColor:
                  index == selectedIndex2 ? '#FA6400' : '#EFF2F4',
                borderRadius: 16,
                padding: 10,
                marginTop: 15,
                marginLeft: 30,
              }}>
              <Text
                style={{
                  color: index == selectedIndex2 ? '#ffffff' : '#333333',
                  fontFamily: 'AvenirLTStd-Medium',
                  fontSize: 13,
                  fontWeight: '500',
                  textAlign: 'center',
                }}>
                {item.name}
              </Text>
            </TouchableOpacity>
          )}
        />
        <Dropdown
          style={styles.drops}
          label="State"
          icon="cheveron-down"
          iconColor="#000000"
          icon={require('../images/drop.png')}
          data={drop}
        />
        <Dropdown
          style={styles.drops}
          label="City"
          icon="cheveron-down"
          iconColor="#000000"
          icon={require('../images/drop.png')}
          data={drop}
        />
        <Dropdown
          style={styles.drops}
          label="School Name"
          icon="cheveron-down"
          iconColor="#000000"
          icon={require('../images/drop.png')}
          data={drop}
        />

        <TouchableRipple
          style={styles.touch}
          onPress={() => profilecheckout()}
          rippleColor="rgba(0, 0, 0, .32)">
          <Text style={styles.touchtext}>Next</Text>
        </TouchableRipple>
      </ScrollView>
    </View>
  );
};

export default CreatePassword;

const styles = StyleSheet.create({
  text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 30,
    fontWeight: 'bold',
    color: '#1E1F20',
    marginHorizontal: 30,
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#1E1F20',
    marginHorizontal: 30,
    marginTop: 20,
  },
  sub2text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#1E1F20',
    marginHorizontal: 30,
    marginTop: 20,
  },
  box: {
    width: 80,
    height: 30,
    borderRadius: 17,
    borderWidth: 1,
    borderColor: '#FA6400',
    backgroundColor: '#FA6400',
    marginHorizontal: 30,
    marginTop: 10,
  },
  item: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 13,
    fontWeight: '500',
    color: '#333333',
    textAlign: 'center',
    // alignSelf: 'center',
    // marginTop: 4,
  },
  box1: {
    width: 80,
    height: 30,
    borderRadius: 17,
    borderWidth: 1,
    borderColor: '#EFF2F4',
    backgroundColor: '#EFF2F4',
    marginHorizontal: 30,
    marginTop: 10,
  },
  boxText1: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 13,
    fontWeight: '500',
    color: '#000000',
    alignSelf: 'center',
    marginTop: 4,
  },
  box2: {
    width: 145,
    height: 30,
    borderRadius: 17,
    borderWidth: 1,
    borderColor: '#EFF2F4',
    backgroundColor: '#EFF2F4',
    marginHorizontal: 30,
    marginTop: 10,
  },
  box3: {
    width: 110,
    height: 30,
    borderRadius: 17,
    borderWidth: 1,
    borderColor: '#EFF2F4',
    backgroundColor: '#EFF2F4',
    marginHorizontal: 30,
    marginTop: 15,
  },
  box4: {
    width: 130,
    height: 30,
    borderRadius: 17,
    borderWidth: 1,
    borderColor: '#EFF2F4',
    backgroundColor: '#EFF2F4',
    marginTop: 15,
    marginLeft: -10,
  },
  box5: {
    width: 60,
    height: 30,
    borderRadius: 17,
    borderWidth: 1,
    borderColor: '#FA6400',
    backgroundColor: '#FA6400',
    marginHorizontal: 30,
    marginTop: 10,
  },
  box6: {
    width: 60,
    height: 30,
    borderRadius: 17,
    borderWidth: 1,
    borderColor: '#EFF2F4',
    backgroundColor: '#EFF2F4',
    marginHorizontal: 30,
    marginTop: 10,
  },
  box7: {
    width: 120,
    height: 30,
    borderRadius: 17,
    borderWidth: 1,
    borderColor: '#FA6400',
    backgroundColor: '#FA6400',
    marginHorizontal: 30,
    marginTop: 10,
  },
  box8: {
    width: 120,
    height: 30,
    borderRadius: 17,
    borderWidth: 1,
    borderColor: '#EFF2F4',
    backgroundColor: '#EFF2F4',
    marginTop: 10,
    marginLeft: -10,
  },
  box9: {
    width: 120,
    height: 30,
    borderRadius: 17,
    borderWidth: 1,
    borderColor: '#EFF2F4',
    backgroundColor: '#EFF2F4',
    marginHorizontal: 30,
    marginTop: 15,
  },
  drops: {
    backgroundColor: '#fff',
    marginHorizontal: 30,
    marginTop: 10,
  },
  touch: {
    padding: 10,
    marginHorizontal: 30,
    borderRadius: 5,
    backgroundColor: '#0253B3',
    marginTop: 20,
    elevation: 15,
    marginBottom: 20,
  },
  touchtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 22,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
  },
});
