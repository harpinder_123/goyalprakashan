import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
const {height} = Dimensions.get('window');

const DragAndDrop = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <ImageBackground
        style={styles.image}
        source={require('../images/bg-12.png')}>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity onPress={() => navigation.navigate('EndExercise')}>
            <Image
              style={styles.subimage}
              source={require('../images/close.png')}
            />
          </TouchableOpacity>
          <Image
            style={styles.endimage}
            source={require('../images/clock.png')}
          />
          <Text style={styles.text}>10:00</Text>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <TouchableOpacity onPress={() => navigation.navigate('Result')}>
            <Image
              style={styles.images}
              source={require('../images/hidecar.png')}
            />
          </TouchableOpacity>
          <Image style={styles.images} source={require('../images/bike.png')} />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Image
            style={styles.images}
            source={require('../images/hidebike.png')}
          />
          <Image
            style={styles.images}
            source={require('../images/whitebus.png')}
          />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Image
            style={styles.images}
            source={require('../images/hidebus.png')}
          />
          <Image style={styles.images} source={require('../images/car2.png')} />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Image
            style={styles.images}
            source={require('../images/hidecar.png')}
          />
          <Image style={styles.images} source={require('../images/bike.png')} />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Image
            style={styles.images}
            source={require('../images/hidebike.png')}
          />
          <Image
            style={styles.images}
            source={require('../images/whitebus.png')}
          />
        </View>
      </ImageBackground>
    </View>
  );
};

export default DragAndDrop;

const styles = StyleSheet.create({
  image: {
    // marginTop: height / 3,
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  subimage: {
    width: 15,
    height: 15,
    marginTop: 60,
    marginHorizontal: 30,
  },
  endimage: {
    width: 15,
    height: 15,
    marginTop: 60,
    marginLeft: 'auto',
  },
  text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    marginTop: 55,
    marginLeft: 5,
    marginHorizontal: 30,
  },
  container: {
    width: 120,
    height: 110,
    backgroundColor: '#00796B',
    borderRadius: 22,
    marginHorizontal: 20,
    marginTop: 30,
    borderWidth: 3,
    borderColor: '#DDDBDB',
  },
  containerText: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#fff',
    textAlign: 'center',
    marginTop: 40,
  },
  images: {
    width: 120,
    height: 65,
    marginTop: 60,
    marginHorizontal: 20,
  },
});
