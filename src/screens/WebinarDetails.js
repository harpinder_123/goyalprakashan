import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderLight} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const WebinarDetails = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <HeaderLight onPress={() => navigation.goBack()} />
      <ScrollView decelerationRate={0.5}>
        <Text style={styles.text}>Live Demo: On24 Webcast Elite</Text>
        <ImageBackground
          style={styles.image}
          source={require('../images/web.png')}></ImageBackground>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            marginHorizontal: 40,
          }}>
          <View>
            <Text style={styles.imagetext}>Date</Text>
            <Text style={styles.image2text}>Sep 28, 2021</Text>
          </View>
          <View
            style={{
              width: 1,
              height: 40,
              borderRadius: 5,
              backgroundColor: '#DBDBDB',
              marginHorizontal: 30,
              marginTop: 20,
            }}
          />
          <View>
            <Text style={styles.imagetext}>Time</Text>
            <Text style={styles.image2text}>10am - 12pm</Text>
          </View>
          <View
            style={{
              width: 1,
              height: 40,
              borderRadius: 5,
              backgroundColor: '#DBDBDB',
              marginHorizontal: 30,
              marginTop: 20,
            }}
          />
          <View>
            <Text style={styles.imagetext}>Duration</Text>
            <Text style={styles.image2text}>40 mins</Text>
          </View>
        </View>
        <Text style={styles.texted}>
          Engaging executives is hard enough. But without face-to-face meetings
          and in-person events it’s even harder. The good news is, in a digital
          world we can build customised experiences that attract and engage
          executive audiences in new and creative ways.
        </Text>
        <View>
          <Text style={styles.headertext}>Speaker</Text>
          <Text style={styles.headersubtext}>
            Mark Bornstein, VP of Marketing at ON24
          </Text>
          <Text style={styles.header2text}>What to expect from webinar</Text>
          <View style={{flexDirection: 'row'}}>
            <View style={styles.serial}>
              <Text style={styles.serialtext}>1</Text>
            </View>
            <Text style={styles.loremText}>
              Lorem Ipsum is simply dummy text of the{`\n`}printing and
              typesetting industry.
            </Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <View style={styles.serial}>
              <Text style={styles.serialtext}>2</Text>
            </View>
            <Text style={styles.loremText}>
              Lorem Ipsum is simply dummy text of the{`\n`}printing and
              typesetting industry.
            </Text>
          </View>
          <View style={{flexDirection: 'row'}}>
            <View style={styles.serial}>
              <Text style={styles.serialtext}>3</Text>
            </View>
            <Text style={styles.loremText}>
              Lorem Ipsum is simply dummy text of the{`\n`}printing and
              typesetting industry.
            </Text>
          </View>
        </View>
        <TouchableOpacity style={styles.touch}>
          <Text style={styles.touchtext}>Book Now</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default WebinarDetails;

const styles = StyleSheet.create({
  image: {
    width: 340,
    height: 150,
    alignSelf: 'center',
    marginTop: 25,
  },
  text: {
    marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#333333',
  },
  imagetext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#333333',
    marginHorizontal: 30,
    marginTop: 20,
    textAlign: 'center',
  },
  image2text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#333333',
    marginHorizontal: 30,
    marginTop: 5,
  },
  subtext: {
    marginTop: 10,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#333333',
    lineHeight: 22,
  },
  touch: {
    padding: 10,
    marginHorizontal: 30,
    borderRadius: 5,
    backgroundColor: '#0253B3',
    marginTop: 40,
    elevation: 15,
    marginBottom: 20,
  },
  touchtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 22,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#dadce0aa',
    marginTop: 15,
    marginHorizontal: 30,
    // marginLeft: 25,
  },
  subImage: {
    width: 104,
    height: 104,
    marginHorizontal: 30,
    marginTop: 20,
    borderRadius: 8,
  },
  inputtxt: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 20,
    marginLeft: -10,
    lineHeight: 22,
  },
  sub2text: {
    fontFamily: 'AvenirLTStd-Normal',
    fontSize: 12,
    fontWeight: '400',
    color: '#999999',
    marginTop: 5,
    marginLeft: -10,
  },
  texted: {
    fontFamily: 'AvenirLTStd-Normal',
    fontSize: 14,
    fontWeight: '400',
    color: '#333333',
    marginTop: 20,
    lineHeight: 22,
    marginHorizontal: 30,
    textAlign: 'justify',
  },
  headertext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#333333',
    marginHorizontal: 30,
    marginTop: 10,
  },
  header2text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#333333',
    marginHorizontal: 30,
    marginTop: 20,
  },
  headersubtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#333333',
    marginHorizontal: 30,
    marginTop: 10,
  },
  serial: {
    width: 30,
    height: 30,
    backgroundColor: '#FA6400',
    borderRadius: 50,
    marginHorizontal: 30,
    marginTop: 20,
  },
  serialtext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#fff',
    textAlign: 'center',
    marginTop: 4,
  },
  loremText: {
    fontFamily: 'AvenirLTStd-Normal',
    fontSize: 14,
    fontWeight: '400',
    color: '#333333',
    marginTop: 15,
    marginLeft: -10,
    lineHeight: 22,
  },
});
