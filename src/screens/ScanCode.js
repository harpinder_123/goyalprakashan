import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderLight} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const ScanCode = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <HeaderLight onPress={() => navigation.goBack()} />
      <Text style={styles.text}>Scan QR Code</Text>
      <Image style={styles.image} source={require('../images/scan2.png')} />
    </View>
  );
};

export default ScanCode;

const styles = StyleSheet.create({
  image: {
    width: 292,
    height: 292,
    alignSelf: 'center',
    marginTop: '40%',
  },
  text: {
    marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
  },
});
