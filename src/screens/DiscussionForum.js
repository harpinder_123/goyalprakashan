import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderLight} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const DiscussionForum = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <HeaderLight onPress={() => navigation.goBack()} />
      <TouchableOpacity
        style={{
          position: 'absolute',
          bottom: 0,
          alignSelf: 'flex-end',
          marginBottom: 40,
        }}>
        <Image
          style={{
            width: 60,
            height: 60,
            marginLeft: 'auto',
            marginHorizontal: 30,
            marginBottom: 0,
          }}
          source={require('../images/Group.png')}
        />
      </TouchableOpacity>
      <ScrollView decelerationRate={0.5}>
        <Text style={styles.text}>Discussion Forum</Text>
        <View style={{flexDirection: 'row'}}>
          <Image style={styles.image} source={require('../images/gamer.png')} />
          <View>
            <Text style={styles.subtext}>Ritu kumar</Text>
            <Text style={styles.sub2text}>2 hours ago</Text>
          </View>
        </View>
        <Text style={styles.sub3text}>What is Lorem Ipsum?</Text>
        <Text style={styles.sub4text}>
          Lorem Ipsum is simply dummy text of the{`\n`}printing and typesetting
          industry…{' '}
        </Text>
        <View style={{flexDirection: 'row'}}>
          <View style={styles.box}>
            <Text style={styles.boxtext}>Maths</Text>
          </View>
          <View style={styles.box2}>
            <Text style={styles.boxtext}>Maths</Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            marginHorizontal: 10,
          }}>
          <View>
            <Image
              style={{
                width: 20,
                height: 18,
                marginTop: 32,
                alignSelf: 'center',
              }}
              source={require('../images/reply.png')}
            />
            <Text
              style={{
                marginTop: 5,
                fontWeight: '500',
                fontFamily: 'AvenirLTStd-Medium',
                fontSize: 14,
                color: '#161F3D',
              }}>
              15
            </Text>
          </View>
          <View>
            <Image
              style={{
                width: 20,
                height: 20,
                marginTop: 30,
                marginLeft: 20,
                alignSelf: 'center',
              }}
              source={require('../images/like.png')}
            />
            <Text
              style={{
                marginLeft: 20,
                marginTop: 5,
                fontWeight: '500',
                fontFamily: 'AvenirLTStd-Medium',
                fontSize: 14,
                color: '#161F3D',
                textAlign: 'center',
              }}>
              Like
            </Text>
          </View>
          <View>
            <Image
              style={{
                width: 20,
                height: 20,
                marginTop: 30,
                marginLeft: 20,
                alignSelf: 'center',
              }}
              source={require('../images/dislike.png')}
            />
            <Text
              style={{
                marginLeft: 20,
                marginTop: 5,
                fontWeight: '500',
                fontFamily: 'AvenirLTStd-Medium',
                fontSize: 14,
                color: '#161F3D',
                textAlign: 'center',
              }}>
              Dislike
            </Text>
          </View>
          <View>
            <Image
              style={{
                width: 20,
                height: 20,
                marginTop: 30,
                marginLeft: 20,
                alignSelf: 'center',
              }}
              source={require('../images/blocked.png')}
            />
            <Text
              style={{
                marginLeft: 20,
                marginTop: 5,
                fontWeight: '500',
                fontFamily: 'AvenirLTStd-Medium',
                fontSize: 14,
                color: '#161F3D',
                textAlign: 'center',
              }}>
              Report
            </Text>
          </View>
        </View>
        <View style={styles.Line} />

        <View style={{flexDirection: 'row'}}>
          <Image style={styles.image} source={require('../images/gamer.png')} />
          <View>
            <Text style={styles.subtext}>Ritu kumar</Text>
            <Text style={styles.sub2text}>2 hours ago</Text>
          </View>
        </View>
        <Text style={styles.sub3text}>What is Lorem Ipsum?</Text>
        <Text style={styles.sub4text}>
          Lorem Ipsum is simply dummy text of the{`\n`}printing and typesetting
          industry…{' '}
        </Text>
        <View style={{flexDirection: 'row'}}>
          <View style={styles.box}>
            <Text style={styles.boxtext}>Maths</Text>
          </View>
          <View style={styles.box2}>
            <Text style={styles.boxtext}>Maths</Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            marginHorizontal: 10,
          }}>
          <View>
            <Image
              style={{
                width: 20,
                height: 18,
                marginTop: 32,
                alignSelf: 'center',
              }}
              source={require('../images/reply.png')}
            />
            <Text
              style={{
                marginTop: 5,
                fontWeight: '500',
                fontFamily: 'AvenirLTStd-Medium',
                fontSize: 14,
                color: '#161F3D',
              }}>
              15
            </Text>
          </View>
          <View>
            <Image
              style={{
                width: 20,
                height: 20,
                marginTop: 30,
                marginLeft: 20,
                alignSelf: 'center',
              }}
              source={require('../images/like.png')}
            />
            <Text
              style={{
                marginLeft: 20,
                marginTop: 5,
                fontWeight: '500',
                fontFamily: 'AvenirLTStd-Medium',
                fontSize: 14,
                color: '#161F3D',
                textAlign: 'center',
              }}>
              Like
            </Text>
          </View>
          <View>
            <Image
              style={{
                width: 20,
                height: 20,
                marginTop: 30,
                marginLeft: 20,
                alignSelf: 'center',
              }}
              source={require('../images/dislike.png')}
            />
            <Text
              style={{
                marginLeft: 20,
                marginTop: 5,
                fontWeight: '500',
                fontFamily: 'AvenirLTStd-Medium',
                fontSize: 14,
                color: '#161F3D',
                textAlign: 'center',
              }}>
              Dislike
            </Text>
          </View>
          <View>
            <Image
              style={{
                width: 20,
                height: 20,
                marginTop: 30,
                marginLeft: 20,
                alignSelf: 'center',
              }}
              source={require('../images/blocked.png')}
            />
            <Text
              style={{
                marginLeft: 20,
                marginTop: 5,
                fontWeight: '500',
                fontFamily: 'AvenirLTStd-Medium',
                fontSize: 14,
                color: '#161F3D',
                textAlign: 'center',
              }}>
              Report
            </Text>
          </View>
        </View>
        <View style={styles.Line} />

        <View style={{flexDirection: 'row'}}>
          <Image style={styles.image} source={require('../images/gamer.png')} />
          <View>
            <Text style={styles.subtext}>Ritu kumar</Text>
            <Text style={styles.sub2text}>2 hours ago</Text>
          </View>
        </View>
        <Text style={styles.sub3text}>What is Lorem Ipsum?</Text>
        <Text style={styles.sub4text}>
          Lorem Ipsum is simply dummy text of the{`\n`}printing and typesetting
          industry…{' '}
        </Text>
        <View style={{flexDirection: 'row'}}>
          <View style={styles.box}>
            <Text style={styles.boxtext}>Maths</Text>
          </View>
          <View style={styles.box2}>
            <Text style={styles.boxtext}>Maths</Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-around',
            marginHorizontal: 10,
          }}>
          <View>
            <Image
              style={{
                width: 20,
                height: 18,
                marginTop: 32,
                alignSelf: 'center',
              }}
              source={require('../images/reply.png')}
            />
            <Text
              style={{
                marginTop: 5,
                fontWeight: '500',
                fontFamily: 'AvenirLTStd-Medium',
                fontSize: 14,
                color: '#161F3D',
              }}>
              15
            </Text>
          </View>
          <View>
            <Image
              style={{
                width: 20,
                height: 20,
                marginTop: 30,
                marginLeft: 20,
                alignSelf: 'center',
              }}
              source={require('../images/like.png')}
            />
            <Text
              style={{
                marginLeft: 20,
                marginTop: 5,
                fontWeight: '500',
                fontFamily: 'AvenirLTStd-Medium',
                fontSize: 14,
                color: '#161F3D',
                textAlign: 'center',
              }}>
              Like
            </Text>
          </View>
          <View>
            <Image
              style={{
                width: 20,
                height: 20,
                marginTop: 30,
                marginLeft: 20,
                alignSelf: 'center',
              }}
              source={require('../images/dislike.png')}
            />
            <Text
              style={{
                marginLeft: 20,
                marginTop: 5,
                fontWeight: '500',
                fontFamily: 'AvenirLTStd-Medium',
                fontSize: 14,
                color: '#161F3D',
                textAlign: 'center',
              }}>
              Dislike
            </Text>
          </View>
          <View>
            <Image
              style={{
                width: 20,
                height: 20,
                marginTop: 30,
                marginLeft: 20,
                alignSelf: 'center',
              }}
              source={require('../images/blocked.png')}
            />
            <Text
              style={{
                marginLeft: 20,
                marginTop: 5,
                fontWeight: '500',
                fontFamily: 'AvenirLTStd-Medium',
                fontSize: 14,
                color: '#161F3D',
                textAlign: 'center',
              }}>
              Report
            </Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default DiscussionForum;

const styles = StyleSheet.create({
  text: {
    marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
  },
  image: {
    width: 40,
    height: 40,
    marginTop: 40,
    marginHorizontal: 30,
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 35,
    marginLeft: -15,
  },
  sub2text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#0000004d',
    marginLeft: -15,
  },
  sub3text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 20,
    marginHorizontal: 30,
  },
  sub4text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: 'grey',
    marginTop: 10,
    marginHorizontal: 30,
    lineHeight: 22,
  },
  profileText: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 40,
    color: '#333333',
    marginLeft: -10,
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#dadce0aa',
    marginTop: 10,
    marginHorizontal: 30,
    marginRight: 30,
  },
  arrow: {
    width: 10,
    height: 17,
    marginTop: 45,
    // marginHorizontal: 'auto',
  },
  box: {
    width: 60,
    height: 25,
    backgroundColor: '#FA6400',
    borderRadius: 12,
    marginHorizontal: 30,
    marginTop: 10,
  },
  box2: {
    width: 60,
    height: 25,
    backgroundColor: '#FA6400',
    borderRadius: 12,
    marginTop: 10,
    marginLeft: -20,
  },
  boxtext: {
    color: '#fff',
    alignSelf: 'center',
    fontSize: 12,
    marginTop: 4,
  },
});
