import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  TextInput,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
const {height} = Dimensions.get('window');

const Search = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <KeyboardAwareScrollView>
        <ImageBackground
          style={styles.image}
          source={require('../images/bg.png')}>
          <View style={styles.container}>
            <View>
              <Image
                style={styles.subimage}
                source={require('../images/search.png')}
              />
              <TextInput
                style={{marginTop: -32, marginLeft: 40}}
                placeholder="Search"
              />
            </View>
          </View>
        </ImageBackground>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={styles.texted}>Search History </Text>
          <Text style={styles.endText}>Clear all</Text>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={styles.untexted}>Maths 10 Book</Text>
          <Image
            style={{width: 8, height: 8, marginTop: 30, marginHorizontal: 30}}
            source={require('../images/close.png')}
          />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={styles.untexted}>Maths 10 Book</Text>
          <Image
            style={{width: 8, height: 8, marginTop: 30, marginHorizontal: 30}}
            source={require('../images/close.png')}
          />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={styles.untexted}>Maths 10 Book</Text>
          <Image
            style={{width: 8, height: 8, marginTop: 30, marginHorizontal: 30}}
            source={require('../images/close.png')}
          />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={styles.untexted}>Maths 10 Book</Text>
          <Image
            style={{width: 8, height: 8, marginTop: 30, marginHorizontal: 30}}
            source={require('../images/close.png')}
          />
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={styles.untexted}>Maths 10 Book</Text>
          <Image
            style={{width: 8, height: 8, marginTop: 30, marginHorizontal: 30}}
            source={require('../images/close.png')}
          />
        </View>
        <Text style={styles.header}>Popular Books</Text>
        <View style={{flexDirection: 'row'}}>
          <Image
            style={styles.sub2Image}
            source={require('../images/book.png')}
          />
          <View>
            <Text style={styles.inputtxt}>Core Composite Mathematics</Text>
            <Text style={styles.sub2text}>
              A Complete Course In Science Lab Manual…
            </Text>
          </View>
        </View>

        <Image
          style={styles.sub6image}
          source={require('../images/star.png')}
        />
        <Image
          style={styles.sub7image}
          source={require('../images/star.png')}
        />
        <Image
          style={styles.sub8image}
          source={require('../images/star.png')}
        />

        <View
          style={{
            flexDirection: 'row',
            marginHorizontal: 60,
          }}>
          <TouchableOpacity style={styles.touch}>
            <Text style={styles.touchtext}>Preview</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.touch2}
            onPress={() => navigation.navigate('OnlinePopup')}>
            <Text style={styles.touchtext}>Activate</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.Line} />
        <View style={{flexDirection: 'row'}}>
          <Image
            style={styles.sub2Image}
            source={require('../images/book.png')}
          />
          <View>
            <Text style={styles.inputtxt}>Core Composite Mathematics</Text>
            <Text style={styles.sub2text}>
              A Complete Course In Science Lab Manual…
            </Text>
          </View>
        </View>

        <Image
          style={styles.sub6image}
          source={require('../images/star.png')}
        />
        <Image
          style={styles.sub7image}
          source={require('../images/star.png')}
        />
        <Image
          style={styles.sub8image}
          source={require('../images/star.png')}
        />

        <View
          style={{
            flexDirection: 'row',
            marginHorizontal: 60,
          }}>
          <TouchableOpacity style={styles.touch}>
            <Text style={styles.touchtext}>Preview</Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.touch2}>
            <Text style={styles.touchtext}>Buy Now</Text>
          </TouchableOpacity>
        </View>
      </KeyboardAwareScrollView>
    </View>
  );
};

export default Search;

const styles = StyleSheet.create({
  image: {
    // marginTop: height / 3,
    width: 380,
    height: 100,
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  container: {
    marginHorizontal: 20,
    backgroundColor: '#fff',
    borderRadius: 25,
    elevation: 5,
    marginTop: 60,
  },
  subimage: {
    width: 16,
    height: 16,
    marginHorizontal: 20,
    marginTop: 15,
  },
  sub2Image: {
    width: 76,
    height: 100,
    marginHorizontal: 30,
    marginTop: 20,
    borderRadius: 8,
  },
  texted: {
    fontFamily: 'AvenirLTStd-Normal',
    fontSize: 14,
    fontWeight: '400',
    marginTop: 30,
    color: '#8A94A3',
    marginHorizontal: 30,
  },
  header: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 30,
    color: '#333333',
    marginHorizontal: 30,
  },
  untexted: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '500',
    marginTop: 30,
    color: '#2A3B56',
    marginHorizontal: 30,
  },
  endText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '500',
    marginTop: 30,
    color: '#8A94A3',
    marginHorizontal: 30,
  },
  inputtxt: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 20,
    marginLeft: -10,
  },
  sub2text: {
    fontFamily: 'AvenirLTStd-Normal',
    fontSize: 12,
    fontWeight: '400',
    color: '#999999',
    marginLeft: -10,
    marginTop: 3,
    // marginLeft: 'auto',
    // marginRight: 30,
  },
  sub6image: {
    marginHorizontal: 125,
    marginTop: -55,
    height: 14,
    width: 14,
  },
  sub7image: {
    marginHorizontal: 140,
    height: 14,
    width: 14,
    marginTop: -14,
  },
  sub8image: {
    marginHorizontal: 155,
    height: 14,
    width: 14,
    marginTop: -14,
  },
  touch: {
    width: 80,
    height: 30,
    borderRadius: 25,
    backgroundColor: '#FA6400',
    marginTop: 10,
    marginLeft: 60,
    // marginBottom: 10,
    // alignSelf: 'center',
  },
  touch2: {
    width: 80,
    height: 30,
    borderRadius: 25,
    backgroundColor: '#0253B3',
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 10,
  },
  touchtext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 12,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 6,
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#1E1F204d',
    marginTop: 7,
    marginHorizontal: 30,
    // marginLeft: 25,
  },
});
