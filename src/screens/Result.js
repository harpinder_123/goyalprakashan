import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
const {height} = Dimensions.get('window');

const Result = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <ImageBackground
        style={styles.image}
        source={require('../images/confet.png')}>
        <Image
          style={styles.subimage}
          source={require('../images/close.png')}
        />
        <Image
          style={styles.checkimage}
          source={require('../images/check.png')}
        />
        <Text style={styles.text}>Thank you for participating!</Text>
        <Text style={styles.subtext}>Following is your score</Text>
        <View style={{flexDirection: 'row'}}>
          <View style={styles.container}>
            <View
              style={{
                width: 41,
                height: 41,
                backgroundColor: '#637170',
                borderRadius: 50,
                alignSelf: 'center',
                marginTop: 10,
              }}>
              <Text style={styles.centertext}>20</Text>
            </View>
            <Text style={styles.containerText}>Total Questions</Text>
          </View>
          <View style={styles.container}>
            <View
              style={{
                width: 41,
                height: 41,
                backgroundColor: '#4BDF00',
                borderRadius: 50,
                alignSelf: 'center',
                marginTop: 10,
              }}>
              <Text style={styles.centertext}>15</Text>
            </View>
            <Text style={styles.containerText}>Total Questions</Text>
          </View>
          <View style={styles.container}>
            <View
              style={{
                width: 41,
                height: 41,
                backgroundColor: '#FF1F1F',
                borderRadius: 50,
                alignSelf: 'center',
                marginTop: 10,
              }}>
              <Text style={styles.centertext}>5</Text>
            </View>
            <Text style={styles.containerText}>Total Questions</Text>
          </View>
        </View>
        <TouchableOpacity
          style={styles.touch}
          onPress={() => navigation.navigate('TabNavigator')}>
          <Text style={styles.touchtext}>Go to Home</Text>
        </TouchableOpacity>
      </ImageBackground>
    </View>
  );
};

export default Result;

const styles = StyleSheet.create({
  image: {
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  subimage: {
    width: 15,
    height: 15,
    marginTop: 60,
    marginHorizontal: 30,
  },
  checkimage: {
    width: 100,
    height: 100,
    marginTop: height / 5,
    alignSelf: 'center',
  },
  text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 20,
    textAlign: 'center',
    color: '#344356',
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 18,
    fontWeight: '700',
    marginTop: 20,
    textAlign: 'center',
    color: '#344356',
  },
  centertext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 20,
    fontWeight: '500',
    marginTop: 5,
    textAlign: 'center',
    color: '#fff',
  },
  container: {
    width: 90,
    height: 110,
    backgroundColor: '#fff',
    borderRadius: 15,
    marginHorizontal: 20,
    marginTop: 20,
    elevation: 5,
  },
  containerText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '700',
    color: '#344356',
    textAlign: 'center',
    marginTop: 10,
  },
  images: {
    width: 120,
    height: 110,
    marginTop: 30,
    marginHorizontal: 90,
  },
  touch: {
    width: 180,
    height: 40,
    borderRadius: 25,
    backgroundColor: '#0253B3',
    marginTop: '50%',
    marginBottom: 20,
    alignSelf: 'center',
  },
  touchtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 8,
  },
});
