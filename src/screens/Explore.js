import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {useNavigation} from '@react-navigation/native';
const {height} = Dimensions.get('window');

const Explore = () => {
  const navigation = useNavigation();
  return (
    <View>
      <View style={{backgroundColor: '#EFF2F4', flex: 1}}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            marginHorizontal: 15,
          }}>
          <TouchableOpacity
            onPress={() => navigation.navigate('SelectSubject')}>
            <View style={styles.box}>
              <Image
                style={{
                  width: 40,
                  height: 40,
                  alignSelf: 'center',
                  marginTop: 15,
                }}
                source={require('../images/content.png')}
              />
              <Text
                style={{
                  textAlign: 'center',
                  marginTop: 15,
                  fontWeight: 'bold',
                }}>
                Study
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('Community')}>
            <View style={styles.box}>
              <Image
                style={{
                  width: 40,
                  height: 40,
                  alignSelf: 'center',
                  marginTop: 15,
                }}
                source={require('../images/chat.png')}
              />
              <Text
                style={{
                  textAlign: 'center',
                  marginTop: 15,
                  fontWeight: 'bold',
                }}>
                Community
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('Updates')}>
            <View style={styles.box}>
              <Image
                style={{
                  width: 40,
                  height: 40,
                  alignSelf: 'center',
                  marginTop: 15,
                }}
                source={require('../images/newspaper.png')}
              />
              <Text
                style={{
                  textAlign: 'center',
                  marginTop: 15,
                  fontWeight: 'bold',
                }}>
                Updates
              </Text>
            </View>
          </TouchableOpacity>
        </View>

        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            marginHorizontal: 15,
          }}>
          <TouchableOpacity>
            <View style={styles.box}>
              <Image
                style={{
                  width: 40,
                  height: 40,
                  alignSelf: 'center',
                  marginTop: 15,
                }}
                source={require('../images/webinar1.png')}
              />
              <Text
                style={{
                  textAlign: 'center',
                  marginTop: 15,
                  fontWeight: 'bold',
                }}>
                Live Classes
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('Webinars')}>
            <View style={styles.box}>
              <Image
                style={{
                  width: 40,
                  height: 40,
                  alignSelf: 'center',
                  marginTop: 15,
                }}
                source={require('../images/webinar.png')}
              />
              <Text
                style={{
                  textAlign: 'center',
                  marginTop: 15,
                  fontWeight: 'bold',
                }}>
                Webinar
              </Text>
            </View>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => navigation.navigate('MiniApps')}>
            <View style={styles.box}>
              <Image
                style={{
                  width: 40,
                  height: 40,
                  alignSelf: 'center',
                  marginTop: 15,
                }}
                source={require('../images/addplus.png')}
              />
              <Text
                style={{
                  textAlign: 'center',
                  marginTop: 15,
                  fontWeight: 'bold',
                }}>
                Mini Apps
              </Text>
            </View>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Explore;

const styles = StyleSheet.create({
  box: {
    width: 100,
    height: 100,
    backgroundColor: '#fff',
    borderRadius: 15,
    marginHorizontal: 10,
    marginTop: 20,
  },
});
