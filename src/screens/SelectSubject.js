import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderLight} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const SelectSubject = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <HeaderLight onPress={() => navigation.goBack()} />
      <Text style={styles.text}>Select Subject</Text>
      <Text style={styles.subtext}>5 Subjects</Text>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-evenly',
          marginHorizontal: 20,
        }}>
        <TouchableOpacity onPress={() => navigation.navigate('Chapter')}>
          <Image
            style={{width: 80, height: 80, marginTop: 40, marginHorizontal: 30}}
            source={require('../images/account.png')}
          />
          <Text style={styles.account}>Accountancy</Text>
        </TouchableOpacity>
        <TouchableOpacity>
          <Image
            style={{width: 80, height: 80, marginTop: 40, marginHorizontal: 30}}
            source={require('../images/science.png')}
          />
          <Text style={styles.account}>Chemistry</Text>
        </TouchableOpacity>
        <TouchableOpacity>
          <Image
            style={{width: 80, height: 80, marginTop: 40, marginHorizontal: 30}}
            source={require('../images/economics.png')}
          />
          <Text style={styles.account}>Economics</Text>
        </TouchableOpacity>
      </View>
      <View
        style={{
          flexDirection: 'row',
        }}>
        <TouchableOpacity>
          <Image
            style={{width: 80, height: 80, marginTop: 40, marginHorizontal: 30}}
            source={require('../images/math.png')}
          />
          <Text style={styles.account}>Maths</Text>
        </TouchableOpacity>
        <TouchableOpacity style={{marginLeft: -15}}>
          <Image
            style={{width: 80, height: 80, marginTop: 40, marginHorizontal: 30}}
            source={require('../images/united-kingdom.png')}
          />
          <Text style={styles.account}>English</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default SelectSubject;

const styles = StyleSheet.create({
  image: {
    width: 50,
    height: 50,
    marginTop: 20,
    marginHorizontal: '25%',
  },
  text: {
    marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
  },
  subtext: {
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#8F92A1',
    marginTop: 5,
  },
  account: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#1F2833',
    marginTop: 5,
    alignSelf: 'center',
  },
  box: {
    padding: 10,
    marginHorizontal: 30,
    backgroundColor: '#EFF2F4',
    borderRadius: 10,
    marginTop: 20,
    flexDirection: 'row',
  },
  boxtext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
    marginHorizontal: 10,
    marginTop: 5,
  },
  subBoxtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#333333',
    lineHeight: 22,
  },
  boximage: {
    width: 8,
    height: 13,
    marginTop: 15,
    marginHorizontal: '15%',
  },
});
