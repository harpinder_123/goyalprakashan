import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
const {height} = Dimensions.get('window');

const TestSummary = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#EFF2F4', flex: 1}}>
      <StatusBarDark />
      <ScrollView decelerationRate={0.5}>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity onPress={() => navigation.navigate('Study2')}>
            <Image
              style={styles.image}
              source={require('../images/close.png')}
            />
          </TouchableOpacity>
          <Text style={styles.text}>Test Summary</Text>
        </View>
        <View style={styles.container}>
          <Text style={styles.containerText}>
            Accounting for Not-for-Profit Organisations
          </Text>
          <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={styles.containerimg}
                source={require('../images/calendar1.png')}
              />
              <Text style={styles.subtext}>8 Sep ‘20</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={styles.containerimg}
                source={require('../images/clock1.png')}
              />
              <Text style={styles.subtext}>10:00</Text>
            </View>
            <View style={{flexDirection: 'row'}}>
              <Image
                style={styles.containerimg}
                source={require('../images/list.png')}
              />
              <Text style={styles.subtext}>50 Question</Text>
            </View>
          </View>
        </View>
        <ImageBackground
          style={{
            width: 335,
            height: 250,
            alignSelf: 'center',
            marginTop: 20,
            borderRadius: 25,
          }}
          source={require('../images/confetti.png')}>
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.sub2text}>
              Hey Pradeep, Keep{`\n`}improving!
            </Text>
            <Image
              style={styles.endimage}
              source={require('../images/medal.png')}
            />
          </View>
          <Text style={styles.sub2text}>Your Score</Text>
          <Text style={styles.sub3text}>40/50</Text>
          <View style={styles.Line} />
          <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
            <View>
              <Text style={styles.bottomtext}>70%</Text>
              <Text style={styles.bottomsubtext}>Accuracy</Text>
            </View>
            <View>
              <Text style={styles.bottomtext}>08:00 mins</Text>
              <Text style={styles.bottomsubtext}>Time</Text>
            </View>
            <View>
              <Text style={styles.bottomtext}>200</Text>
              <Text style={styles.bottomsubtext}>Rank</Text>
            </View>
          </View>
        </ImageBackground>
        <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
          <View style={styles.box}>
            <View
              style={{
                width: 41,
                height: 41,
                backgroundColor: '#637170',
                borderRadius: 50,
                alignSelf: 'center',
                marginTop: 10,
              }}>
              <Text style={styles.boxtext}>50</Text>
            </View>
            <Text style={styles.box2}>Total{`\n`}Questions</Text>
          </View>
          <View style={styles.box}>
            <View
              style={{
                width: 41,
                height: 41,
                backgroundColor: '#6DD400',
                borderRadius: 50,
                alignSelf: 'center',
                marginTop: 10,
              }}>
              <Text style={styles.boxtext}>40</Text>
            </View>
            <Text style={styles.box2}>
              Attempted
              {`\n`}Questions
            </Text>
          </View>
          <View style={styles.box}>
            <View
              style={{
                width: 41,
                height: 41,
                backgroundColor: '#E02020',
                borderRadius: 50,
                alignSelf: 'center',
                marginTop: 10,
              }}>
              <Text style={styles.boxtext}>10</Text>
            </View>
            <Text style={styles.box2}>Unattempted{`\n`}Questions</Text>
          </View>
        </View>
        <View style={{flexDirection: 'row', marginHorizontal: 25}}>
          <View style={styles.box}>
            <View
              style={{
                width: 41,
                height: 41,
                backgroundColor: '#6DD400',
                borderRadius: 50,
                alignSelf: 'center',
                marginTop: 10,
              }}>
              <Text style={styles.boxtext}>35</Text>
            </View>
            <Text style={styles.box2}>Total{`\n`}Questions</Text>
          </View>
          <View style={{marginLeft: 20}}>
            <View style={styles.box}>
              <View
                style={{
                  width: 41,
                  height: 41,
                  backgroundColor: '#E02020',
                  borderRadius: 50,
                  alignSelf: 'center',
                  marginTop: 10,
                }}>
                <Text style={styles.boxtext}>5</Text>
              </View>
              <Text style={styles.box2}>
                Attempted
                {`\n`}Questions
              </Text>
            </View>
          </View>
        </View>
        <TouchableOpacity
          style={styles.touch}
          onPress={() => navigation.navigate('Solutions')}>
          <Text style={styles.touchtext}>View Solutions</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default TestSummary;

const styles = StyleSheet.create({
  image: {
    width: 15,
    height: 15,
    marginTop: 60,
    marginHorizontal: 30,
  },
  text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 50,
    alignSelf: 'center',
    marginHorizontal: 60,
    color: '#333333',
  },
  container: {
    padding: 10,
    marginHorizontal: 30,
    backgroundColor: '#ffffff',
    borderRadius: 8,
    marginTop: 30,
  },
  containerText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#273253',
    marginHorizontal: 10,
  },
  containerimg: {
    width: 16,
    height: 16,
    marginTop: 20,
    marginHorizontal: 10,
    marginBottom: 5,
  },
  endimage: {
    width: 40,
    height: 50,
    marginTop: 20,
    marginLeft: 'auto',
    marginHorizontal: 20,
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#273253',
    marginTop: 18,
  },
  sub2text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#ffffffaa',
    marginTop: 20,
    marginHorizontal: 20,
    lineHeight: 25,
  },
  sub3text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#ffffff',
    marginTop: 5,
    marginHorizontal: 20,
  },
  box: {
    width: 100,
    height: 110,
    backgroundColor: '#fff',
    borderRadius: 15,
    // marginHorizontal: 20,
    marginTop: 20,
    elevation: 5,
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#dadce04d',
    opacity: 0.5,
    marginTop: 15,
    marginHorizontal: 20,
    // marginLeft: 25,
  },
  bottomtext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#ffffff',
    marginTop: 20,
    marginHorizontal: 30,
    textAlign: 'center',
  },
  bottomsubtext: {
    fontFamily: 'AvenirLTStd-Normal',
    fontSize: 15,
    fontWeight: '500',
    color: '#ffffffaa',
    marginTop: 5,
    marginHorizontal: 30,
    textAlign: 'center',
  },
  boxtext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 5,
    textAlign: 'center',
    color: '#fff',
  },
  box2: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#344356',
    textAlign: 'center',
    marginTop: 10,
  },
  box2text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#0253B3',
    marginHorizontal: 10,
  },
  touch: {
    width: 190,
    height: 40,
    borderRadius: 25,
    backgroundColor: '#0253B3',
    marginTop: 30,
    alignSelf: 'center',
    elevation: 5,
    marginBottom: 20,
  },
  touchtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 8,
  },
});
