import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {TouchableRipple} from 'react-native-paper';
import {HeaderTop} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const BookDetails = ({navigation}) => {
  const bookcheckout = () => {
    setTimeout(() => {
      navigation.navigate('Accountancy');
    }, 300);
  };
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <ScrollView decelerationRate={0.5}>
        <StatusBarDark />
        <HeaderTop onPress={() => navigation.goBack()} />
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.Toptext}>
            A Complete Course in{`\n`}Mathematics
          </Text>
          <TouchableOpacity>
            <Image
              style={styles.image}
              source={require('../images/share.png')}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            marginLeft: -20,
          }}>
          <View>
            <Text style={styles.text}>ICSE</Text>
            <Text style={styles.subtext}>Board</Text>
          </View>
          <View>
            <Text style={styles.text}>12</Text>
            <Text style={styles.subtext}>Standard</Text>
          </View>
          <View>
            <Text style={styles.text}>Mathematics</Text>
            <Text style={styles.subtext}>Subject</Text>
          </View>
          <View>
            <Text style={styles.text}>B K SINGH</Text>
            <Text style={styles.subtext}>Author</Text>
          </View>
        </View>
        <View style={styles.Line} />
        <View>
          <Text style={styles.HeaderText}>About the Book</Text>
          <Text style={styles.SubHeaderText}>
            The book entitled A Complete Course in Mathematics for Class X has
            been written strictly according to the latest Textbook published by
            NCERT and as per the latest..
          </Text>
          <Text style={styles.read}>Read more…</Text>
        </View>
        <View style={styles.Line} />
        <View>
          <Text style={styles.HeaderText}>About the Book</Text>
          <Text style={styles.SubHeaderText}>
            The book entitled A Complete Course in Mathematics for Class X has
            been written strictly according to the latest Textbook published by
            NCERT and as per the latest..
          </Text>
          <Text style={styles.read}>Read more…</Text>
        </View>
        <TouchableRipple
          style={styles.touch}
          onPress={() => bookcheckout()}
          rippleColor="rgba(0, 0, 0, .32)">
          <Text style={styles.touchtext}>Start Learning</Text>
        </TouchableRipple>
      </ScrollView>
    </View>
  );
};

export default BookDetails;

const styles = StyleSheet.create({
  image: {
    width: 30,
    height: 30,
    marginTop: 50,
    marginLeft: 80,
  },
  Toptext: {
    marginTop: 40,
    marginHorizontal: 20,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#333333',
    lineHeight: 30,
  },
  text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 13,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 20,
    alignSelf: 'center',
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Normal',
    fontSize: 13,
    fontWeight: '400',
    color: '#8F8F8F',
    marginHorizontal: 10,
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#dadce0aa',
    marginTop: 15,
    marginHorizontal: 20,
  },
  HeaderText: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 10,
    marginHorizontal: 20,
  },
  SubHeaderText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 13,
    fontWeight: '500',
    color: '#333333',
    marginTop: 5,
    marginHorizontal: 20,
    lineHeight: 20,
  },
  read: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 13,
    fontWeight: 'bold',
    color: '#0253B3',
    marginTop: 5,
    marginHorizontal: 20,
  },
  touch: {
    padding: 10,
    marginHorizontal: 30,
    borderRadius: 5,
    backgroundColor: '#0253B3',
    marginTop: '20%',
    elevation: 15,
    marginBottom: 20,
  },
  touchtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 20,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
  },
});
