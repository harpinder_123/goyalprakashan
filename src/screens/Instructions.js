import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderLight} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const Instructions = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <HeaderLight onPress={() => navigation.goBack()} />
      <ScrollView decelerationRate={0.5}>
        <Text style={styles.subtext}>V - Maths </Text>
        <Text style={styles.text}>Match the column</Text>
        <Text style={styles.sub2text}>
          This is a matching Introduction/Review game to help students remember
          the properties.
        </Text>
        <View style={{flexDirection: 'row'}}>
          <View style={{flexDirection: 'row'}}>
            <Image style={styles.mark} source={require('../images/mark.png')} />
            <Text style={styles.marktext}>10</Text>
            <Text style={styles.markSubtext}>Num. Tries</Text>
          </View>
          <View style={{flexDirection: 'row', marginHorizontal: 40}}>
            <Image
              style={styles.mark}
              source={require('../images/hourglass.png')}
            />
            <Text style={styles.marktext}>15 min</Text>
            <Text style={styles.markSub2text}>Maximum time</Text>
          </View>
        </View>
        <Text style={styles.texted}>Instructions</Text>
        <View style={{flexDirection: 'row'}}>
          <View style={styles.serial}>
            <Text style={styles.serialtext}>1</Text>
          </View>
          <Text style={styles.loremText}>
            Lorem Ipsum is simply dummy text of the{`\n`}printing and
            typesetting industry.
          </Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <View style={styles.serial}>
            <Text style={styles.serialtext}>2</Text>
          </View>
          <Text style={styles.loremText}>
            Lorem Ipsum is simply dummy text of the{`\n`}printing and
            typesetting industry.
          </Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <View style={styles.serial}>
            <Text style={styles.serialtext}>3</Text>
          </View>
          <Text style={styles.loremText}>
            Lorem Ipsum is simply dummy text of the{`\n`}printing and
            typesetting industry.
          </Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <View style={styles.serial}>
            <Text style={styles.serialtext}>4</Text>
          </View>
          <Text style={styles.loremText}>
            Lorem Ipsum is simply dummy text of the{`\n`}printing and
            typesetting industry.
          </Text>
        </View>
        <TouchableOpacity
          style={styles.touch}
          onPress={() => navigation.navigate('ColomnMatch')}>
          <Text style={styles.touchtext}>Start</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default Instructions;

const styles = StyleSheet.create({
  text: {
    marginTop: 5,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#333333',
  },
  texted: {
    marginTop: 30,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#333333',
  },
  subtext: {
    marginTop: 30,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#8F92A1',
  },
  sub2text: {
    marginTop: 5,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#333333',
    lineHeight: 20,
  },
  mark: {
    width: 22,
    height: 40,
    marginTop: 30,
    marginHorizontal: 30,
  },
  marktext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 20,
    marginLeft: -10,
  },
  markSubtext: {
    fontFamily: 'AvenirLTStd-Normal',
    fontSize: 15,
    fontWeight: '400',
    color: 'grey',
    marginTop: 55,
    marginLeft: -30,
  },
  markSub2text: {
    fontFamily: 'AvenirLTStd-Normal',
    fontSize: 15,
    fontWeight: '400',
    color: 'grey',
    marginTop: 55,
    marginLeft: -80,
  },
  serial: {
    width: 30,
    height: 30,
    backgroundColor: '#FA6400',
    borderRadius: 50,
    marginHorizontal: 30,
    marginTop: 20,
  },
  serialtext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#fff',
    textAlign: 'center',
    marginTop: 4,
  },
  loremText: {
    fontFamily: 'AvenirLTStd-Normal',
    fontSize: 14,
    fontWeight: '400',
    color: '#333333',
    marginTop: 15,
    marginLeft: -10,
    lineHeight: 22,
  },
  touch: {
    padding: 10,
    marginHorizontal: 30,
    borderRadius: 5,
    backgroundColor: '#0253B3',
    marginTop: '30%',
    elevation: 15,
    marginBottom: 20,
  },
  touchtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 22,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
  },
});
