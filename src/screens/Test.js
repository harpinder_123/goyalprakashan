import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {useNavigation} from '@react-navigation/native';
const {height} = Dimensions.get('window');

const Test = () => {
  const navigation = useNavigation();
  return (
    <ScrollView horizontal decelerationRate={0.5}>
      <View style={{flexDirection: 'row'}}>
        <TouchableOpacity onPress={() => navigation.navigate('TestDetails')}>
          <ImageBackground
            style={{
              width: 221,
              height: 135,
              marginTop: 20,
              marginHorizontal: 15,
            }}
            source={require('../images/wave.png')}>
            <Text style={styles.dialogText}>
              Accountancy Online Mock Test - XII
            </Text>
            <View
              style={{
                width: 47,
                height: 25,
                backgroundColor: '#FA6400',
                borderRadius: 5,
                marginHorizontal: 15,
                marginTop: 10,
              }}>
              <Text
                style={{
                  color: '#fff',
                  alignSelf: 'center',
                  fontSize: 12,
                  marginTop: 4,
                }}>
                FREE
              </Text>
            </View>
            <Text style={styles.dialogSub2Text}>Test on Sep 25, 10:00 AM</Text>
          </ImageBackground>
        </TouchableOpacity>
        <TouchableOpacity>
          <ImageBackground
            style={{
              width: 221,
              height: 135,
              marginTop: 20,
              marginHorizontal: 15,
            }}
            source={require('../images/wave.png')}>
            <Text style={styles.dialogText}>
              Economics Online Mock{`\n`}Test - XII
            </Text>
            <View
              style={{
                width: 47,
                height: 25,
                backgroundColor: '#5DB400',
                borderRadius: 5,
                marginHorizontal: 15,
                marginTop: 10,
              }}>
              <Text
                style={{
                  color: '#fff',
                  alignSelf: 'center',
                  fontSize: 12,
                  marginTop: 4,
                }}>
                PAID
              </Text>
            </View>
            <Text style={styles.dialogSub2Text}>Test on Sep 25, 10:00 AM</Text>
          </ImageBackground>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

export default Test;

const styles = StyleSheet.create({
  dialogText: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    marginTop: 10,
    color: '#333333',
    marginHorizontal: 10,
  },
  dialogSub2Text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 13,
    fontWeight: '500',
    marginTop: 20,
    color: '#333333',
    marginHorizontal: 10,
  },
});
