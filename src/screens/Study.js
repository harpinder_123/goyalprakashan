import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderLight} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const Study = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <HeaderLight onPress={() => navigation.goBack()} />
      <TouchableOpacity>
        <Image
          style={styles.image}
          source={require('../images/searchbar.png')}
        />
      </TouchableOpacity>
      <ScrollView decelerationRate={0.5}>
        <Text style={styles.text}>Receipts and Payments A/c</Text>
        <Text style={styles.subtext}>Lesson Plan</Text>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity onPress={() => navigation.navigate('Lesson')}>
            <View style={styles.box}>
              <Image
                style={{
                  width: 50,
                  height: 50,
                  alignSelf: 'center',
                  marginTop: 25,
                }}
                source={require('../images/pdf.png')}
              />
            </View>
            <Text style={styles.boxtext}>
              Income & Expenditure{`\n`}Account
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={{marginLeft: -40}}>
            <View style={styles.box}>
              <Image
                style={{
                  width: 50,
                  height: 50,
                  alignSelf: 'center',
                  marginTop: 25,
                }}
                source={require('../images/pdf.png')}
              />
            </View>
            <Text style={styles.boxtext}>
              Income & Expenditure{`\n`}Account
            </Text>
          </TouchableOpacity>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={styles.texted}>Video Lectures</Text>
          <Text style={styles.endText}>View all</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
          }}>
          <ScrollView horizontal>
            <TouchableOpacity
              onPress={() => navigation.navigate('VideoLecture')}>
              <ImageBackground
                style={styles.video}
                source={require('../images/Group2.png')}>
                <Image
                  style={styles.play}
                  source={require('../images/play.png')}
                />
              </ImageBackground>
              <Text style={styles.playtext}>
                How to find and submit{'\n'} quotation?
              </Text>
              <Text style={styles.subplaytext}>10 min</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{marginLeft: -40}}
              onPress={() => navigation.navigate('Subscription')}>
              <ImageBackground
                style={styles.video}
                source={require('../images/Group2.png')}>
                <Image
                  style={styles.lock}
                  source={require('../images/lock.png')}
                />
                <Image
                  style={styles.play2}
                  source={require('../images/play.png')}
                />
              </ImageBackground>
              <Text style={styles.playtext}>
                How to find and submit{'\n'} quotation?
              </Text>
              <Text style={styles.subplaytext}>10 min</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{marginLeft: -40}}>
              <ImageBackground
                style={styles.video}
                source={require('../images/Group2.png')}>
                <Image
                  style={styles.play}
                  source={require('../images/play.png')}
                />
              </ImageBackground>
              <Text style={styles.playtext}>
                How to find and submit{'\n'} quotation?
              </Text>
              <Text style={styles.subplaytext}>10 min</Text>
            </TouchableOpacity>
          </ScrollView>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={styles.texted}>Worksheet</Text>
          <Text style={styles.endText}>View all</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <ScrollView horizontal>
            <TouchableOpacity onPress={() => navigation.navigate('Lesson1')}>
              <View style={styles.box}>
                <Image
                  style={{
                    width: 50,
                    height: 50,
                    alignSelf: 'center',
                    marginTop: 25,
                  }}
                  source={require('../images/pdf.png')}
                />
              </View>
              <Text style={styles.boxtext}>
                Income & Expenditure{`\n`}Account
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={{marginLeft: -40}}
              onPress={() => navigation.navigate('Subscription')}>
              <View style={styles.box}>
                <Image
                  style={{
                    width: 25,
                    height: 25,
                    marginLeft: 'auto',
                    marginHorizontal: 5,
                    marginTop: 5,
                  }}
                  source={require('../images/lock.png')}
                />
                <Image
                  style={{
                    width: 50,
                    height: 50,
                    alignSelf: 'center',
                  }}
                  source={require('../images/pdf.png')}
                />
              </View>
              <Text style={styles.boxtext}>
                Income & Expenditure{`\n`}Account
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{marginLeft: -40}}>
              <View style={styles.box}>
                <Image
                  style={{
                    width: 50,
                    height: 50,
                    alignSelf: 'center',
                    marginTop: 25,
                  }}
                  source={require('../images/pdf.png')}
                />
              </View>
              <Text style={styles.boxtext}>
                Income & Expenditure{`\n`}Account
              </Text>
            </TouchableOpacity>
          </ScrollView>
        </View>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={styles.texted}>Assignments</Text>
          <Text style={styles.endText}>View all</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <ScrollView horizontal>
            <TouchableOpacity>
              <View style={styles.box2}>
                <Image
                  style={{
                    width: 50,
                    height: 50,
                    alignSelf: 'center',
                    marginTop: 25,
                  }}
                  source={require('../images/assignment.png')}
                />
              </View>
              <Text style={styles.boxtext}>
                Income & Expenditure{`\n`}Account
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{marginLeft: -40}}>
              <View style={styles.box2}>
                <Image
                  style={{
                    width: 50,
                    height: 50,
                    alignSelf: 'center',
                    marginTop: 25,
                  }}
                  source={require('../images/assignment.png')}
                />
              </View>
              <Text style={styles.boxtext}>
                Income & Expenditure{`\n`}Account
              </Text>
            </TouchableOpacity>
            <TouchableOpacity style={{marginLeft: -40}}>
              <View style={styles.box2}>
                <Image
                  style={{
                    width: 50,
                    height: 50,
                    alignSelf: 'center',
                    marginTop: 25,
                  }}
                  source={require('../images/assignment.png')}
                />
              </View>
              <Text style={styles.boxtext}>
                Income & Expenditure{`\n`}Account
              </Text>
            </TouchableOpacity>
          </ScrollView>
        </View>
        <Text style={styles.texted}>Flash Cards</Text>

        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity onPress={() => navigation.navigate('Flash')}>
            <View style={styles.box3}>
              <Image
                style={{
                  width: 50,
                  height: 50,
                  alignSelf: 'center',
                  marginTop: 25,
                }}
                source={require('../images/flash.png')}
              />
            </View>
            <Text style={styles.boxtext}>
              Income & Expenditure{`\n`}Account
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={{marginLeft: -40}}>
            <View style={styles.box3}>
              <Image
                style={{
                  width: 50,
                  height: 50,
                  alignSelf: 'center',
                  marginTop: 25,
                }}
                source={require('../images/flash.png')}
              />
            </View>
            <Text style={styles.boxtext}>
              Income & Expenditure{`\n`}Account
            </Text>
          </TouchableOpacity>
        </View>
        <Text style={styles.texted}>Online Test</Text>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity
            onPress={() => navigation.navigate('TestInstructions')}>
            <View style={styles.box4}>
              <Image
                style={{
                  width: 50,
                  height: 50,
                  alignSelf: 'center',
                  marginTop: 25,
                }}
                source={require('../images/test2.png')}
              />
            </View>
            <Text style={styles.boxtext}>
              Income & Expenditure{`\n`}Account
            </Text>
          </TouchableOpacity>
          <TouchableOpacity style={{marginLeft: -40}}>
            <View style={styles.box4}>
              <Image
                style={{
                  width: 50,
                  height: 50,
                  alignSelf: 'center',
                  marginTop: 25,
                }}
                source={require('../images/test2.png')}
              />
            </View>
            <Text style={styles.boxtext}>
              Income & Expenditure{`\n`}Account
            </Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default Study;

const styles = StyleSheet.create({
  image: {
    width: 60,
    height: 60,
    marginLeft: 'auto',
    marginTop: -60,
    marginHorizontal: 20,
  },
  text: {
    marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#333333',
    marginHorizontal: 30,
    marginTop: 20,
  },
  box: {
    width: 140,
    height: 100,
    marginHorizontal: 30,
    backgroundColor: '#F9EED7',
    borderRadius: 15,
    marginTop: 20,
  },
  box2: {
    width: 140,
    height: 100,
    marginHorizontal: 30,
    backgroundColor: '#5B6871',
    borderRadius: 15,
    marginTop: 20,
  },
  box3: {
    width: 140,
    height: 100,
    marginHorizontal: 30,
    backgroundColor: '#E5E1E5',
    borderRadius: 15,
    marginTop: 20,
  },
  box4: {
    width: 140,
    height: 100,
    marginHorizontal: 30,
    backgroundColor: '#FDBF43',
    borderRadius: 15,
    marginTop: 20,
  },
  boxtext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#333333',
    marginHorizontal: 35,
    marginTop: 10,
  },
  texted: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 20,
    color: '#1E1F20',
    marginHorizontal: 30,
  },
  endText: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    marginTop: 20,
    color: '#0253B3',
    marginHorizontal: 20,
  },
  video: {
    width: 140,
    height: 100,
    marginHorizontal: 30,
    marginTop: 10,
    backgroundColor: '#0000004d',
    borderRadius: 15,
    elevation: 5,
  },
  lock: {
    width: 25,
    height: 25,
    marginLeft: 'auto',
    marginHorizontal: 5,
    marginTop: 5,
  },
  play: {
    width: 30,
    height: 30,
    alignSelf: 'center',
    marginTop: 35,
  },
  play2: {
    width: 30,
    height: 30,
    alignSelf: 'center',
    marginTop: 5,
  },
  playtext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 12,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 5,
    marginHorizontal: 35,
  },
  subplaytext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 11,
    fontWeight: 'bold',
    color: '#3333334d',
    marginTop: 5,
    marginHorizontal: 35,
  },
});
