import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
const {height} = Dimensions.get('window');

const ColomnMatch = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <ImageBackground
        style={styles.image}
        source={require('../images/bg-11.jpg')}>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity onPress={() => navigation.navigate('EndExercise')}>
            <Image
              style={styles.subimage}
              source={require('../images/close.png')}
            />
          </TouchableOpacity>
          <Image
            style={styles.endimage}
            source={require('../images/clock.png')}
          />
          <Text style={styles.text}>10:00</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity onPress={() => navigation.navigate('Result')}>
            <View style={styles.container}>
              <Text style={styles.containerText}>CAR</Text>
            </View>
          </TouchableOpacity>
          <Image style={styles.images} source={require('../images/boat.png')} />
        </View>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity>
            <View style={styles.container}>
              <Text style={styles.containerText}>TRAIN</Text>
            </View>
          </TouchableOpacity>
          <Image style={styles.images} source={require('../images/car.png')} />
        </View>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity>
            <View style={styles.container}>
              <Text style={styles.containerText}>BOAT</Text>
            </View>
          </TouchableOpacity>
          <Image
            style={styles.images}
            source={require('../images/cycle.png')}
          />
        </View>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity>
            <View style={styles.container}>
              <Text style={styles.containerText}>BICYCLE</Text>
            </View>
          </TouchableOpacity>
          <Image
            style={styles.images}
            source={require('../images/train.png')}
          />
        </View>
        <View style={{flexDirection: 'row'}}>
          <TouchableOpacity>
            <View style={styles.container}>
              <Text style={styles.containerText}>BUS</Text>
            </View>
          </TouchableOpacity>
          <Image style={styles.images} source={require('../images/bus.png')} />
        </View>
      </ImageBackground>
    </View>
  );
};

export default ColomnMatch;

const styles = StyleSheet.create({
  image: {
    // marginTop: height / 3,
    width: '100%',
    height: '100%',
    resizeMode: 'contain',
    alignSelf: 'center',
  },
  subimage: {
    width: 15,
    height: 15,
    marginTop: 60,
    marginHorizontal: 30,
  },
  endimage: {
    width: 15,
    height: 15,
    marginTop: 60,
    marginLeft: 'auto',
  },
  text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    marginTop: 55,
    marginLeft: 5,
    marginHorizontal: 30,
  },
  container: {
    width: 120,
    height: 110,
    backgroundColor: '#00796B',
    borderRadius: 22,
    marginHorizontal: 20,
    marginTop: 30,
    borderWidth: 3,
    borderColor: '#DDDBDB',
  },
  containerText: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#fff',
    textAlign: 'center',
    marginTop: 40,
  },
  images: {
    width: 120,
    height: 110,
    marginTop: 30,
    marginHorizontal: 90,
  },
});
