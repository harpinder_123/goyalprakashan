import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderLight} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const Webinars = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <HeaderLight onPress={() => navigation.goBack()} />
      <ScrollView decelerationRate={0.5}>
        <Text style={styles.text}>Webinars</Text>
        <ImageBackground
          style={styles.image}
          source={require('../images/web.png')}></ImageBackground>
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.subtext}>Live Demo: On24 Webcast{`\n`}Elite</Text>
          <TouchableOpacity>
            <View style={styles.touch}>
              <Text style={styles.touchText}>Join Now</Text>
            </View>
          </TouchableOpacity>
        </View>
        <View style={styles.Line} />
        <View style={{flexDirection: 'row'}}>
          <Image
            style={styles.subImage}
            source={require('../images/image.png')}
          />
          <View>
            <Text style={styles.inputtxt}>
              Live Demo: On24 Webcast{`\n`}Elite
            </Text>
            <Text style={styles.sub2text}>Date: Sep 28, 2021</Text>
            <TouchableOpacity
              onPress={() => navigation.navigate('WebinarDetails')}>
              <View style={styles.touch2}>
                <Text style={styles.touch2Text}>Book Now</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.Line} />
        <View style={{flexDirection: 'row'}}>
          <Image
            style={styles.subImage}
            source={require('../images/image2.png')}
          />
          <View>
            <Text style={styles.inputtxt}>
              Live Demo: On24 Webcast{`\n`}Elite
            </Text>
            <Text style={styles.sub2text}>Date: Sep 28, 2021</Text>
            <TouchableOpacity>
              <View style={styles.touch2}>
                <Text style={styles.touch2Text}>Book Now</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.Line} />
        <View style={{flexDirection: 'row'}}>
          <Image
            style={styles.subImage}
            source={require('../images/image2.png')}
          />
          <View>
            <Text style={styles.inputtxt}>
              Live Demo: On24 Webcast{`\n`}Elite
            </Text>
            <Text style={styles.sub2text}>Date: Sep 28, 2021</Text>
            <TouchableOpacity>
              <View style={styles.touch2}>
                <Text style={styles.touch2Text}>Book Now</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default Webinars;

const styles = StyleSheet.create({
  image: {
    width: 340,
    height: 150,
    alignSelf: 'center',
    marginTop: 25,
  },
  text: {
    marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
  },
  subtext: {
    marginTop: 10,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#333333',
    lineHeight: 22,
  },
  touch: {
    width: 90,
    height: 30,
    backgroundColor: '#FA6400',
    borderRadius: 15,
    marginTop: 15,
    marginHorizontal: 20,
  },
  touchText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#ffffff',
    textAlign: 'center',
    marginTop: 4,
  },
  touch2: {
    width: 90,
    height: 30,
    backgroundColor: '#0253B3',
    borderRadius: 15,
    marginTop: 10,
    marginLeft: -10,
  },
  touch2Text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#ffffff',
    textAlign: 'center',
    marginTop: 4,
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#dadce0aa',
    marginTop: 15,
    marginHorizontal: 30,
    // marginLeft: 25,
  },
  subImage: {
    width: 104,
    height: 104,
    marginHorizontal: 30,
    marginTop: 20,
    borderRadius: 8,
  },
  inputtxt: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 20,
    marginLeft: -10,
    lineHeight: 22,
  },
  sub2text: {
    fontFamily: 'AvenirLTStd-Normal',
    fontSize: 12,
    fontWeight: '400',
    color: '#999999',
    marginTop: 5,
    marginLeft: -10,
  },
});
