import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderLight} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const ActivePlan = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <HeaderLight onPress={() => navigation.goBack()} />
      <Text style={styles.text}>Active Plan</Text>
      <ImageBackground
        style={{
          width: 340,
          height: 325,
          alignSelf: 'center',
          marginTop: 20,
        }}
        imageStyle={{borderRadius: 15}}
        source={require('../images/bg2.png')}>
        <View style={{flexDirection: 'row'}}>
          <View>
            <Text style={styles.text2}>Basic</Text>
            <Text style={styles.subtext}>₹ 2100/-</Text>
          </View>
          <Image style={styles.image} source={require('../images/tick.png')} />
        </View>
        <View style={{flexDirection: 'row'}}>
          <Image
            style={{width: 20, height: 20, marginTop: 20, marginHorizontal: 30}}
            source={require('../images/check.png')}
          />
          <Text style={styles.sub2text}>Online Test</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Image
            style={{width: 20, height: 20, marginTop: 20, marginHorizontal: 30}}
            source={require('../images/check.png')}
          />
          <Text style={styles.sub2text}>Video Lectures</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Image
            style={{width: 20, height: 20, marginTop: 20, marginHorizontal: 30}}
            source={require('../images/check.png')}
          />
          <Text style={styles.sub2text}>Worksheet</Text>
        </View>
        <View style={{flexDirection: 'row'}}>
          <Image
            style={{width: 20, height: 20, marginTop: 20, marginHorizontal: 30}}
            source={require('../images/check.png')}
          />
          <Text style={styles.sub2text}>Assignments</Text>
        </View>
        <View style={styles.Line} />
        <View style={{flexDirection: 'row', justifyContent: 'space-evenly'}}>
          <View>
            <Text style={styles.bottomtext}>Purchased on</Text>
            <Text style={styles.bottomsubtext}>1 May 2021</Text>
          </View>
          <View>
            <Text style={styles.bottomtext}>Valid Till</Text>
            <Text style={styles.bottomsubtext}>1 June 2021</Text>
          </View>
          <View>
            <Text style={styles.bottomtext}>Days Left</Text>
            <Text style={styles.bottomsubtext}>24</Text>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};

export default ActivePlan;

const styles = StyleSheet.create({
  image: {
    width: 60,
    height: 60,
    marginTop: 20,
    marginLeft: 'auto',
    marginHorizontal: 30,
  },
  text: {
    marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
  },
  text2: {
    marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 22,
    fontWeight: 'bold',
    color: '#ffffff',
  },
  subtext: {
    marginTop: 5,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#ffffff',
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#dadce04d',
    opacity: 0.5,
    marginTop: 15,
    // marginHorizontal: 30,
    // marginLeft: 25,
  },
  subImage: {
    width: 104,
    height: 104,
    marginHorizontal: 30,
    marginTop: 20,
    borderRadius: 8,
  },
  sub2text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#ffffff',
    marginTop: 17,
    marginLeft: -15,
  },
  bottomtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#ffffff',
    marginTop: 10,
    marginHorizontal: 10,
    textAlign: 'center',
  },
  bottomsubtext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#ffffff',
    marginTop: 5,
    marginHorizontal: 10,
    textAlign: 'center',
  },
});
