import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {TextField} from 'react-native-material-textfield';
import {HeaderLight} from '../Custom/CustomView';
const {height} = Dimensions.get('window');
export const CustomInputField = props => (
  <>
    <View style={styles.middleView}>
      <TextField
        containerStyle={styles.tfStyle}
        disableUnderline={true}
        tintColor="#8D92A3"
        {...props}
      />
    </View>
  </>
);

const ProfileDetails = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <ScrollView decelerationRate={0.5}>
        <HeaderLight onPress={() => navigation.goBack()} />
        <Text style={styles.text}>Profile Details</Text>
        <Image
          style={{width: 100, height: 100, alignSelf: 'center', marginTop: 20}}
          source={require('../images/gamer.png')}
        />

        <CustomInputField
          label="Username"
          //   defaultValue={state.password}
          useNativeDriver={true}
        />
        <CustomInputField
          label="Name"
          //   defaultValue={state.password}
          useNativeDriver={true}
        />
        <CustomInputField
          label="Email"
          //   defaultValue={state.password}
          useNativeDriver={true}
        />

        <CustomInputField
          label="Mobile"
          //   defaultValue={state.emailphone}
          useNativeDriver={true}
          keyboardType={'number-pad'}
          maxLength={10}
        />
        <TouchableOpacity
          style={styles.touch}
          onPress={() => navigation.navigate('MyAccount')}>
          <Text style={styles.touchText}>Update</Text>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default ProfileDetails;

const styles = StyleSheet.create({
  image: {
    width: 340,
    height: 150,
    alignSelf: 'center',
    marginTop: 25,
  },
  text: {
    marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
  },
  subtext: {
    marginTop: 10,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#333333',
    lineHeight: 22,
  },
  middleView: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 20,
  },
  tfStyle: {
    width: '80%',
    marginHorizontal: 30,
  },
  touch: {
    padding: 10,
    marginHorizontal: 30,
    borderRadius: 5,
    backgroundColor: '#0253B3',
    marginTop: '40%',
    elevation: 15,
    marginBottom: 20,
  },
  touchText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#ffffff',
    textAlign: 'center',
  },
  touch2: {
    width: 90,
    height: 30,
    backgroundColor: '#0253B3',
    borderRadius: 15,
    marginTop: 10,
    marginLeft: -10,
  },
  touch2Text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#ffffff',
    textAlign: 'center',
    marginTop: 4,
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#dadce0aa',
    marginTop: 15,
    marginHorizontal: 30,
    // marginLeft: 25,
  },
  subImage: {
    width: 104,
    height: 104,
    marginHorizontal: 30,
    marginTop: 20,
    borderRadius: 8,
  },
  inputtxt: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 20,
    marginLeft: -10,
    lineHeight: 22,
  },
  sub2text: {
    fontFamily: 'AvenirLTStd-Normal',
    fontSize: 12,
    fontWeight: '400',
    color: '#999999',
    marginTop: 5,
    marginLeft: -10,
  },
});
