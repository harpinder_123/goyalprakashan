import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {TouchableRipple} from 'react-native-paper';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderTop} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const ExploreDetails = ({navigation}) => {
  const explorecheckout = () => {
    setTimeout(() => {
      navigation.navigate('Accountancy');
    }, 300);
  };
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <ScrollView decelerationRate={0.5}>
        <StatusBarDark />
        <HeaderTop onPress={() => navigation.goBack()} />
        <View style={{flexDirection: 'row'}}>
          <Text style={styles.Toptext}>
            A Complete Course in{`\n`}Mathematics
          </Text>
          <TouchableOpacity>
            <Image
              style={styles.image}
              source={require('../images/preview.png')}
            />
          </TouchableOpacity>
          <TouchableOpacity>
            <Image
              style={styles.image2}
              source={require('../images/share.png')}
            />
          </TouchableOpacity>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            marginLeft: -20,
          }}>
          <View>
            <Text style={styles.text}>ICSE</Text>
            <Text style={styles.subtext}>Board</Text>
          </View>
          <View>
            <Text style={styles.text}>12</Text>
            <Text style={styles.subtext}>Standard</Text>
          </View>
          <View>
            <Text style={styles.text}>Mathematics</Text>
            <Text style={styles.subtext}>Subject</Text>
          </View>
          <View>
            <Text style={styles.text}>B K SINGH</Text>
            <Text style={styles.subtext}>Author</Text>
          </View>
        </View>
        <View style={styles.Line} />
        <View>
          <Text style={styles.HeaderText}>About the Book</Text>
          <Text style={styles.SubHeaderText}>
            The book entitled A Complete Course in Mathematics for Class X has
            been written strictly according to the latest Textbook published by
            NCERT and as per the latest..
          </Text>
          <Text style={styles.read}>Read more…</Text>
        </View>
        <View style={styles.Line} />
        <View>
          <Text style={styles.HeaderText}>About the Book</Text>
          <Text style={styles.SubHeaderText}>
            The book entitled A Complete Course in Mathematics for Class X has
            been written strictly according to the latest Textbook published by
            NCERT and as per the latest..
          </Text>
          <Text style={styles.read}>Read more…</Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            marginHorizontal: 10,
          }}>
          <TouchableOpacity
            style={styles.touch2}
            onPress={() => navigation.navigate('Accountancy')}>
            <Text style={styles.touch2text}>Get E-book</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.touch3}
            onPress={() => navigation.navigate('Accountancy')}>
            <Text style={styles.touch3text}>Get Physical Book</Text>
          </TouchableOpacity>
        </View>
        <TouchableRipple
          style={styles.touch}
          onPress={() => explorecheckout()}
          rippleColor="rgba(0, 0, 0, .32)">
          <Text style={styles.touchtext}>Start Learning</Text>
        </TouchableRipple>
      </ScrollView>
    </View>
  );
};

export default ExploreDetails;

const styles = StyleSheet.create({
  image: {
    width: 30,
    height: 30,
    marginTop: 50,
    marginLeft: 60,
  },
  image2: {
    width: 30,
    height: 30,
    marginTop: 50,
    marginHorizontal: 10,
  },
  Toptext: {
    marginTop: 40,
    marginHorizontal: 20,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#333333',
    lineHeight: 30,
  },
  text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 13,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 20,
    alignSelf: 'center',
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Normal',
    fontSize: 13,
    fontWeight: '400',
    color: '#8F8F8F',
    marginHorizontal: 10,
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#dadce0aa',
    marginTop: 15,
    marginHorizontal: 20,
  },
  HeaderText: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 10,
    marginHorizontal: 20,
  },
  SubHeaderText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 13,
    fontWeight: '500',
    color: '#333333',
    marginTop: 5,
    marginHorizontal: 20,
    lineHeight: 20,
  },
  read: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 13,
    fontWeight: 'bold',
    color: '#FA6400',
    marginTop: 5,
    marginHorizontal: 20,
  },
  touch: {
    padding: 10,
    marginHorizontal: 30,
    borderRadius: 5,
    backgroundColor: '#0253B3',
    marginTop: '5%',
    elevation: 15,
    marginBottom: 20,
  },
  touchtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 20,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
  },
  touch2: {
    width: 160,
    height: 50,
    borderRadius: 5,
    backgroundColor: '#FA6400',
    marginTop: '10%',
    elevation: 15,
  },
  touch2text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 17,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 12,
  },
  touch3: {
    width: 160,
    height: 50,
    borderRadius: 5,
    backgroundColor: '#6D7278',
    marginTop: '10%',
    elevation: 15,
  },
  touch3text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 17,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 12,
  },
});
