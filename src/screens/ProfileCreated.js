import React, {useState} from 'react';
import {
  Dimensions,
  Image,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusBar';
import {Header, MainView} from '../Custom/CustomView';
import {TouchableRipple} from 'react-native-paper';

const {height, width} = Dimensions.get('window');
const ProfileCreated = ({navigation}) => {
  const [modalOpen, setModalOpen] = useState(true);

  const profilecheckout = () => {
    setTimeout(() => {
      navigation.navigate('TabNavigator');
    }, 300);
  };

  return (
    <Modal visible={modalOpen} transparent={true}>
      <View style={styles.modal_View}>
        <View style={styles.mdtop}>
          <Image source={require('../images/tick.png')} style={styles.image} />

          <Text style={styles.text}>
            Your Profile Created {`\n`}Successfully!
          </Text>
          <TouchableRipple
            style={styles.touch}
            onPress={() => profilecheckout()}
            rippleColor="#0253B34d">
            <Text style={styles.touchtext}>Next</Text>
          </TouchableRipple>
        </View>
      </View>
    </Modal>
  );
};

export default ProfileCreated;

const styles = StyleSheet.create({
  modal_View: {
    backgroundColor: '#000000aa',
    flex: 1,
  },
  mdtop: {
    backgroundColor: '#FFFFFF',
    marginTop: height / 3,
    marginHorizontal: 20,
    borderRadius: 20,
  },
  image: {
    width: 80,
    height: 80,
    alignSelf: 'center',
    marginTop: 20,
  },
  text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#1E2432',
    textAlign: 'center',
    marginTop: 10,
    lineHeight: 35,
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#838383',
    textAlign: 'center',
    marginTop: 20,
    marginBottom: 20,
  },
  touch: {
    // width: 135,
    // height: 40,
    borderRadius: 25,
    backgroundColor: '#0253B3',
    marginTop: 20,
    marginBottom: 20,
    alignSelf: 'center',
    paddingHorizontal: 40,
    elevation: 15,
  },
  touchtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
    marginBottom: 8,
    marginTop: 8,
  },
});
