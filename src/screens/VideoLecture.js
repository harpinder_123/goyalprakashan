import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderDark} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const VideoLecture = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <ScrollView decelerationRate={0.5}>
        <HeaderDark onPress={() => navigation.goBack()} />
        {/* <Image
          style={styles.Videoimage}
          source={require('../images/screen-video.png')}
        /> */}
        <Text style={styles.text}>NPO (Meaning & Characteristics)</Text>
        <Text style={styles.subtext}>
          Accounting for Not-for-Profit Organisations
        </Text>
        <View style={{flexDirection: 'row'}}>
          <View>
            <Image
              style={{
                width: 70,
                height: 70,
                marginTop: 20,
                marginHorizontal: 30,
              }}
              source={require('../images/rbookmark.png')}
            />
            <Text style={styles.imgtext}>Bookmark</Text>
          </View>
          <View>
            <Image
              style={{
                width: 70,
                height: 70,
                marginTop: 20,
                marginHorizontal: -10,
              }}
              source={require('../images/download.png')}
            />
            <Text style={styles.img2text}>Download</Text>
          </View>
        </View>
        <Text style={styles.text}>Related Videos</Text>
        <View style={{flexDirection: 'row'}}>
          <ImageBackground
            style={{
              width: 130,
              height: 80,
              marginTop: 20,
              marginHorizontal: 30,
              borderRadius: 15,
            }}
            source={require('../images/Group2.png')}>
            <Image
              style={{
                width: 30,
                height: 30,
                alignSelf: 'center',
                marginTop: 20,
              }}
              source={require('../images/play.png')}
            />
          </ImageBackground>
          <View>
            <Text style={styles.bannertext}>
              NPO (Meaning &{`\n`}Characteristics)
            </Text>
            <Text style={styles.banner2text}>Playing now.....</Text>
          </View>
        </View>
        <View style={{flexDirection: 'row'}}>
          <ImageBackground
            style={{
              width: 130,
              height: 80,
              marginTop: 20,
              marginHorizontal: 30,
              borderRadius: 15,
            }}
            source={require('../images/Group2.png')}>
            <Image
              style={{
                width: 30,
                height: 30,
                alignSelf: 'center',
                marginTop: 20,
              }}
              source={require('../images/play.png')}
            />
          </ImageBackground>
          <View>
            <Text style={styles.bannertext}>
              NPO (Meaning &{`\n`}Characteristics)
            </Text>
            <Text style={styles.banner3text}>10 min</Text>
          </View>
        </View>
        <View style={{flexDirection: 'row'}}>
          <ImageBackground
            style={{
              width: 130,
              height: 80,
              marginTop: 20,
              marginHorizontal: 30,
              borderRadius: 15,
            }}
            source={require('../images/Group2.png')}>
            <Image
              style={{
                width: 30,
                height: 30,
                alignSelf: 'center',
                marginTop: 20,
              }}
              source={require('../images/play.png')}
            />
          </ImageBackground>
          <View>
            <Text style={styles.bannertext}>
              NPO (Meaning &{`\n`}Characteristics)
            </Text>
            <Text style={styles.banner3text}>10 min</Text>
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

export default VideoLecture;

const styles = StyleSheet.create({
  Videoimage: {
    width: 405,
    height: height / 2.3,
    marginHorizontal: -10,
    backgroundColor: 'gray',
  },
  image: {
    width: 50,
    height: 50,
    marginTop: 20,
    marginHorizontal: '25%',
  },
  subImage: {
    width: 101,
    height: 144,
    marginHorizontal: 30,
    marginTop: 20,
  },
  text: {
    marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 18,
    fontWeight: 'bold',
    color: '#333333',
  },
  subtext: {
    marginTop: 5,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#333333',
  },
  imgtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#333333',
    marginHorizontal: 35,
    marginTop: -10,
  },
  bannertext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 20,
    marginLeft: -10,
  },
  banner2text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 13,
    fontWeight: 'bold',
    color: '#FA6400',
    marginTop: 5,
    marginLeft: -10,
  },
  banner3text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 13,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 5,
    marginLeft: -10,
  },
  img2text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 12,
    fontWeight: '500',
    color: '#333333',
    // marginHorizontal: 20,
    marginTop: -10,
  },
  box: {
    padding: 10,
    marginHorizontal: 30,
    backgroundColor: '#EFF2F4',
    borderRadius: 10,
    marginTop: 20,
    flexDirection: 'row',
  },
  boxtext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
    marginHorizontal: 10,
    marginTop: 5,
  },
  subBoxtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#333333',
    lineHeight: 22,
  },
  boximage: {
    width: 8,
    height: 13,
    marginTop: 15,
    marginHorizontal: '15%',
  },
});

// import React, {useState} from 'react';
// import {
//   Text,
//   View,
//   Button,
//   Image,
//   StyleSheet,
//   Platform,
//   NetInfo,
//   Alert,
//   FlatList,
//   SafeAreaView,
//   TextInput,
//   ScrollView,
// } from 'react-native';
// const VideoLecture = ({navigation}) => {
//   return (
//     <View>
//       <Text>Hello</Text>
//     </View>
//   );
// };

// export default VideoLecture;

// const styles = StyleSheet.create({
//   backgroundVideo: {
//     position: 'absolute',
//     top: 0,
//     left: 0,
//     bottom: 0,
//     right: 0,
//   },
// });
