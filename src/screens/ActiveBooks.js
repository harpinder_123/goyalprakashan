import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderLight} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const ActiveBooks = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <HeaderLight onPress={() => navigation.goBack()} />
      <TouchableOpacity onPress={() => navigation.navigate('OnlineSupport')}>
        <Image style={styles.image} source={require('../images/add.png')} />
      </TouchableOpacity>
      <ScrollView decelerationRate={0.5}>
        <Text style={styles.text}>Active books</Text>
        <View style={{flexDirection: 'row'}}>
          <Image
            style={styles.subImage}
            source={require('../images/book.png')}
          />
          <Text style={styles.inputtxt}>
            A Complete Course{`\n`}in Mathematics
          </Text>
          <Image
            style={{width: 30, height: 30, marginTop: 20, marginHorizontal: 40}}
            source={require('../images/share.png')}
          />
        </View>
        <Image
          style={styles.sub3image}
          source={require('../images/star.png')}
        />
        <Image
          style={styles.sub4image}
          source={require('../images/star.png')}
        />
        <Image
          style={styles.sub5image}
          source={require('../images/star.png')}
        />
        <Text style={styles.subtext}>A Complete Course In Science…</Text>
        <Text style={styles.sub2text}>Validity: 20 Days Left</Text>

        <View
          style={{
            flexDirection: 'row',
            marginHorizontal: 70,
          }}>
          <TouchableOpacity
            style={styles.touch}
            onPress={() => navigation.navigate('Accountancy')}>
            <Text style={styles.touchtext}>Learn</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.touch2}
            onPress={() => navigation.navigate('BookDetails')}>
            <Text style={styles.touchtext}>Details</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.Line} />
        <View style={{flexDirection: 'row'}}>
          <Image
            style={styles.subImage}
            source={require('../images/book2.png')}
          />
          <Text style={styles.inputtxt}>
            A Complete Course{`\n`}in Mathematics
          </Text>
          <Image
            style={{width: 30, height: 30, marginTop: 20, marginHorizontal: 40}}
            source={require('../images/share.png')}
          />
        </View>
        <Image
          style={styles.sub3image}
          source={require('../images/star.png')}
        />
        <Image
          style={styles.sub4image}
          source={require('../images/star.png')}
        />
        <Image
          style={styles.sub5image}
          source={require('../images/star.png')}
        />
        <Text style={styles.subtext}>A Complete Course In Science…</Text>
        <Text style={styles.sub2text}>Validity: 20 Days Left</Text>

        <View
          style={{
            flexDirection: 'row',
            marginHorizontal: 70,
          }}>
          <TouchableOpacity style={styles.touch}>
            <Text style={styles.touchtext}>Learn</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.touch2}
            onPress={() => navigation.navigate('Success')}>
            <Text style={styles.touchtext}>Details</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.Line} />
        <View style={{flexDirection: 'row'}}>
          <Image
            style={styles.subImage}
            source={require('../images/book.png')}
          />
          <Text style={styles.inputtxt}>
            A Complete Course{`\n`}in Mathematics
          </Text>
          <Image
            style={{width: 30, height: 30, marginTop: 20, marginHorizontal: 40}}
            source={require('../images/share.png')}
          />
        </View>
        <Image
          style={styles.sub3image}
          source={require('../images/star.png')}
        />
        <Image
          style={styles.sub4image}
          source={require('../images/star.png')}
        />
        <Image
          style={styles.sub5image}
          source={require('../images/star.png')}
        />
        <Text style={styles.subtext}>A Complete Course In Science…</Text>
        <Text style={styles.sub2text}>Validity: 20 Days Left</Text>

        <View
          style={{
            flexDirection: 'row',
            marginHorizontal: 70,
          }}>
          <TouchableOpacity style={styles.touch}>
            <Text style={styles.touchtext}>Learn</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.touch2}
            onPress={() => navigation.navigate('Success')}>
            <Text style={styles.touchtext}>Details</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.Line} />
      </ScrollView>
    </View>
  );
};

export default ActiveBooks;

const styles = StyleSheet.create({
  image: {
    width: 65,
    height: 65,
    marginLeft: 'auto',
    marginTop: -65,
    marginHorizontal: 20,
  },
  subImage: {
    width: 101,
    height: 144,
    marginHorizontal: 30,
    marginTop: 20,
  },
  text: {
    marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
  },
  inputtxt: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 20,
    marginLeft: -10,
  },
  sub3image: {
    marginHorizontal: 150,
    marginTop: -100,
    height: 14,
    width: 14,
  },
  sub4image: {
    marginHorizontal: 165,
    height: 14,
    width: 14,
    marginTop: -14,
  },
  sub5image: {
    marginHorizontal: 180,
    height: 14,
    width: 14,
    marginTop: -14,
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Normal',
    fontSize: 12,
    fontWeight: '400',
    color: '#8A94A3',
    marginTop: 5,
    marginLeft: 155,
  },
  sub2text: {
    fontFamily: 'AvenirLTStd-Normal',
    fontSize: 12,
    fontWeight: '400',
    color: '#FA6400',
    marginTop: 5,
    marginLeft: 155,
  },
  touch: {
    width: 80,
    height: 30,
    borderRadius: 25,
    backgroundColor: '#0253B3',
    marginTop: 15,
    marginLeft: 80,
    // marginBottom: 10,
    // alignSelf: 'center',
  },
  touch2: {
    width: 80,
    height: 30,
    borderRadius: 25,
    backgroundColor: '#6D7278',
    marginTop: 15,
    marginBottom: 10,
    marginLeft: 10,
    // alignSelf: 'center',
  },
  touchtext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 5,
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#dadce0aa',
    marginTop: 7,
    marginHorizontal: 30,
    // marginLeft: 25,
  },
});
