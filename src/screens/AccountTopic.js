import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {TextField} from 'react-native-material-textfield';
import {HeaderLight} from '../Custom/CustomView';
const {height} = Dimensions.get('window');
export const CustomInputField = props => (
  <>
    <View style={styles.middleView}>
      <TextField
        containerStyle={styles.tfStyle}
        disableUnderline={true}
        tintColor="#8D92A3"
        {...props}
      />
    </View>
  </>
);

const AccountTopic = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <HeaderLight onPress={() => navigation.goBack()} />

      <Text style={styles.text}>
        Accounting for Not-for-Profit Organisations
      </Text>
      <Text style={styles.subtext}>3 Topic</Text>
      <TouchableOpacity onPress={() => navigation.navigate('AccountSubTopic')}>
        <View style={styles.box}>
          <Text style={styles.boxtext}>01</Text>
          <Text style={styles.subBoxtext}>Receipts and Payments A/c</Text>
          <Image
            style={styles.boximage}
            source={require('../images/arrow-back.png')}
          />
        </View>
      </TouchableOpacity>
      <TouchableOpacity>
        <View style={styles.box}>
          <Text style={styles.boxtext}>02</Text>
          <Text style={styles.subBoxtext}>Income and Expenditure A/c</Text>
          <Image
            style={styles.boximage}
            source={require('../images/arrow-back.png')}
          />
        </View>
      </TouchableOpacity>
      <TouchableOpacity>
        <View style={styles.box}>
          <Text style={styles.boxtext}>03</Text>
          <Text style={styles.subBoxtext}>Balance Sheet</Text>
          <Image
            style={styles.boximage}
            source={require('../images/arrow-back.png')}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default AccountTopic;

const styles = StyleSheet.create({
  image: {
    width: 50,
    height: 50,
    marginTop: 20,
    marginHorizontal: '25%',
  },
  text: {
    marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
  },
  subtext: {
    marginTop: 5,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#8F92A1',
    lineHeight: 25,
  },
  box: {
    padding: 15,
    marginHorizontal: 30,
    backgroundColor: '#EFF2F4',
    borderRadius: 10,
    marginTop: 20,
    flexDirection: 'row',
  },
  boxtext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
    marginHorizontal: 10,
    marginTop: 5,
  },
  subBoxtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#333333',
    lineHeight: 22,
    marginTop: 10,
  },
  boximage: {
    width: 8,
    height: 13,
    marginTop: 15,
    marginLeft: 'auto',
    marginHorizontal: 10,
  },
});
