import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {useNavigation} from '@react-navigation/native';
const {height} = Dimensions.get('window');

const Excercises = () => {
  const navigation = useNavigation();
  return (
    <ScrollView horizontal decelerationRate={0.5}>
      <View style={{flexDirection: 'row'}}>
        <TouchableOpacity onPress={() => navigation.navigate('Instructions')}>
          <View style={styles.dialogBox}>
            <Image
              style={{width: 221, height: 110}}
              source={require('../images/Rectangle.png')}
            />
            <Text style={styles.dialogText}>Match the column</Text>
            <Text style={styles.dialogSubText}>V - Maths </Text>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Instructions2')}>
          <View style={styles.dialogBox}>
            <Image
              style={{width: 221, height: 110}}
              source={require('../images/Rectangle.png')}
            />
            <Text style={styles.dialogText}>Drag and Drop</Text>
            <Text style={styles.dialogSubText}>III - English </Text>
          </View>
        </TouchableOpacity>
      </View>
    </ScrollView>
  );
};

export default Excercises;

const styles = StyleSheet.create({
  dialogBox: {
    width: 221,
    height: 180,
    backgroundColor: '#fff',
    borderRadius: 15,
    marginHorizontal: 15,
    marginTop: 20,
  },
  dialogText: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    marginTop: 10,
    color: '#333333',
    marginHorizontal: 10,
  },
  dialogSubText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 12,
    fontWeight: '500',
    marginTop: 5,
    color: '#333333',
    marginHorizontal: 10,
  },
});
