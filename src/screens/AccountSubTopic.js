import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderLight} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const AccountSubTopic = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <HeaderLight onPress={() => navigation.goBack()} />

      <Text style={styles.text}>Receipts and Payments A/c</Text>
      <Text style={styles.subtext}>2 Sub Topic</Text>
      <TouchableOpacity onPress={() => navigation.navigate('Study')}>
        <View style={styles.box}>
          <Text style={styles.boxtext}>01</Text>
          <Text style={styles.subBoxtext}>Sub Topic Heading</Text>
          <Image
            style={styles.boximage}
            source={require('../images/arrow-back.png')}
          />
        </View>
      </TouchableOpacity>
      <TouchableOpacity>
        <View style={styles.box}>
          <Text style={styles.boxtext}>02</Text>
          <Text style={styles.subBoxtext}>Sub Topic Heading</Text>
          <Image
            style={styles.boximage}
            source={require('../images/arrow-back.png')}
          />
        </View>
      </TouchableOpacity>
    </View>
  );
};

export default AccountSubTopic;

const styles = StyleSheet.create({
  image: {
    width: 50,
    height: 50,
    marginTop: 20,
    marginHorizontal: '25%',
  },
  text: {
    marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
  },
  subtext: {
    marginTop: 5,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 15,
    fontWeight: '500',
    color: '#8F92A1',
    lineHeight: 25,
  },
  box: {
    padding: 15,
    marginHorizontal: 30,
    backgroundColor: '#EFF2F4',
    borderRadius: 10,
    marginTop: 20,
    flexDirection: 'row',
  },
  boxtext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
    marginHorizontal: 10,
    marginTop: 5,
  },
  subBoxtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#333333',
    lineHeight: 22,
    marginTop: 10,
  },
  boximage: {
    width: 8,
    height: 13,
    marginTop: 15,
    marginLeft: 'auto',
    marginHorizontal: 10,
  },
});
