import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {Header} from '../Custom/CustomView';
import OTPInputView from '@twotalltotems/react-native-otp-input';
const {height} = Dimensions.get('window');

const Otp = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <Header onPress={() => navigation.goBack()} />
      <Text style={styles.text}>Phone Verification</Text>
      <Text style={styles.subtext}>
        We have sent you a code to verify your phone number
      </Text>
      <OTPInputView
        style={styles.otpInput}
        pinCount={4}
        // code={state.otp}
        autoFocusOnLoad
        codeInputFieldStyle={styles.underlineStyleBase}
        codeInputHighlightStyle={styles.underlineStyleHighLighted}
      />
      <Text style={styles.bottom}>
        I did not recieve a code?{' '}
        <TouchableOpacity>
          <Text
            style={{
              color: '#FA6400',
            }}>
            RESEND
          </Text>
        </TouchableOpacity>
      </Text>
      <View style={styles.container}>
        <Text style={styles.containerText}>GET ON CALL</Text>
      </View>
      <TouchableOpacity
        style={styles.touch}
        onPress={() => navigation.navigate('CreateProfile')}>
        <Text style={styles.touchtext}>Verify</Text>
      </TouchableOpacity>
    </View>
  );
};

export default Otp;

const styles = StyleSheet.create({
  text: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 30,
    fontWeight: 'bold',
    marginTop: 10,
    color: '#1E1F20',
    marginHorizontal: 30,
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#8F92A1',
    marginHorizontal: 30,
    lineHeight: 25,
    marginTop: 5,
  },
  otpInput: {
    height: 60,
    marginHorizontal: 40,
    marginTop: 60,
  },
  underlineStyleBase: {
    width: 60,
    height: 60,
    borderWidth: 1,
    borderColor: '#3443564d',
    backgroundColor: '#fff',
    borderRadius: 20,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 24,
    fontWeight: 'bold',
    color: '#100C08',
  },

  underlineStyleHighLighted: {
    width: 60,
    height: 60,
    borderRadius: 20,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 24,
    fontWeight: 'bold',
    color: 'white',
    backgroundColor: '#fff',
  },
  bottom: {
    textAlign: 'center',
    marginTop: 20,
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#8F92A1',
  },
  container: {
    width: 150,
    height: 40,
    borderColor: '#FA6400',
    borderRadius: 5,
    borderWidth: 2,
    alignSelf: 'center',
    marginTop: 20,
  },
  containerText: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#FA6400',
    textAlign: 'center',
    marginTop: 7,
  },
  touch: {
    padding: 10,
    marginHorizontal: 30,
    borderRadius: 5,
    backgroundColor: '#0253B3',
    marginTop: 40,
    elevation: 15,
  },
  touchtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 22,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
  },
});
