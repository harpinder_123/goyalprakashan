import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderLight} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const MiniApps = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <HeaderLight onPress={() => navigation.goBack()} />
      <Text style={styles.text}>Mini Apps</Text>

      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 70, height: 70, marginTop: 20, marginHorizontal: 30}}
          source={require('../images/minin.png')}
        />

        <Text style={styles.inputtxt}>
          Merit Box -
          <Text
            style={{
              fontFamily: 'AvenirLTStd-Medium',
              fontWeight: '500',
              color: '#1F2833',
            }}>
            {' '}
            Online{`\n`}Support | CBSE ICSE{`\n`}NCERT
          </Text>
        </Text>
        <Image
          style={{
            width: 30,
            height: 30,
            alignSelf: 'center',
            marginHorizontal: 35,
            marginLeft: 'auto',
            marginTop: 10,
          }}
          source={require('../images/google-play.png')}
        />
      </View>
      <View style={styles.Line} />
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 70, height: 70, marginTop: 20, marginHorizontal: 30}}
          source={require('../images/minin.png')}
        />

        <Text style={styles.inputtxt}>
          Merit Box -
          <Text
            style={{
              fontFamily: 'AvenirLTStd-Medium',
              fontWeight: '500',
              color: '#1F2833',
            }}>
            {' '}
            Online{`\n`}Support | CBSE ICSE{`\n`}NCERT
          </Text>
        </Text>
        <Image
          style={{
            width: 30,
            height: 30,
            alignSelf: 'center',
            marginHorizontal: 35,
            marginLeft: 'auto',
            marginTop: 10,
          }}
          source={require('../images/google-play.png')}
        />
      </View>
      <View style={styles.Line} />
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 70, height: 70, marginTop: 20, marginHorizontal: 30}}
          source={require('../images/minin.png')}
        />

        <Text style={styles.inputtxt}>
          Merit Box -
          <Text
            style={{
              fontFamily: 'AvenirLTStd-Medium',
              fontWeight: '500',
              color: '#1F2833',
            }}>
            {' '}
            Online{`\n`}Support | CBSE ICSE{`\n`}NCERT
          </Text>
        </Text>
        <Image
          style={{
            width: 30,
            height: 30,
            alignSelf: 'center',
            marginHorizontal: 35,
            marginLeft: 'auto',
            marginTop: 10,
          }}
          source={require('../images/google-play.png')}
        />
      </View>
      <View style={styles.Line} />
      <View style={{flexDirection: 'row'}}>
        <Image
          style={{width: 70, height: 70, marginTop: 20, marginHorizontal: 30}}
          source={require('../images/minin.png')}
        />

        <Text style={styles.inputtxt}>
          Merit Box -
          <Text
            style={{
              fontFamily: 'AvenirLTStd-Medium',
              fontWeight: '500',
              color: '#1F2833',
            }}>
            {' '}
            Online{`\n`}Support | CBSE ICSE{`\n`}NCERT
          </Text>
        </Text>
        <Image
          style={{
            width: 30,
            height: 30,
            alignSelf: 'center',
            marginHorizontal: 35,
            marginLeft: 'auto',
            marginTop: 10,
          }}
          source={require('../images/google-play.png')}
        />
      </View>
      <View style={styles.Line} />
    </View>
  );
};

export default MiniApps;

const styles = StyleSheet.create({
  image: {
    width: 340,
    height: 150,
    alignSelf: 'center',
    marginTop: 25,
  },
  text: {
    marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
  },
  subtext: {
    marginTop: 10,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#333333',
    lineHeight: 22,
  },
  touch: {
    width: 90,
    height: 30,
    backgroundColor: '#FA6400',
    borderRadius: 15,
    marginTop: 15,
    marginHorizontal: 20,
  },
  touchText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#ffffff',
    textAlign: 'center',
    marginTop: 4,
  },
  touch2: {
    width: 90,
    height: 30,
    backgroundColor: '#0253B3',
    borderRadius: 15,
    marginTop: 10,
    marginLeft: -10,
  },
  touch2Text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#ffffff',
    textAlign: 'center',
    marginTop: 4,
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#dadce0aa',
    marginTop: 15,
    marginHorizontal: 30,
    // marginLeft: 25,
  },
  subImage: {
    width: 104,
    height: 104,
    marginHorizontal: 30,
    marginTop: 20,
    borderRadius: 8,
  },
  inputtxt: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 20,
    marginLeft: -10,
    lineHeight: 22,
  },
  sub2text: {
    fontFamily: 'AvenirLTStd-Normal',
    fontSize: 12,
    fontWeight: '400',
    color: '#999999',
    marginTop: 5,
    marginLeft: -10,
  },
});
