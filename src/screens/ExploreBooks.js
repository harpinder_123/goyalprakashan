import React, {useEffect, useState, useRef} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderLight} from '../Custom/CustomView';
import RBSheet from 'react-native-raw-bottom-sheet';
import {RadioButton} from 'react-native-paper';
const {height} = Dimensions.get('window');

const ExploreBooks = ({navigation}) => {
  const refRBSheet = useRef();
  const [checked, setChecked] = useState('first');
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <HeaderLight onPress={() => navigation.goBack()} />
      <TouchableOpacity onPress={() => refRBSheet.current.open()}>
        <Image style={styles.image} source={require('../images/copy.png')} />
      </TouchableOpacity>
      <View style={{flex: 1}}>
        <RBSheet
          ref={refRBSheet}
          closeOnDragDown={true}
          closeOnPressMask={false}
          height={530}
          openDuration={250}
          customStyles={{
            wrapper: {
              backgroundColor: '#0000004d',
            },
            draggableIcon: {
              backgroundColor: '#D8D8D8',
            },
            container: {
              backgroundColor: '#fff',
              borderRadius: 35,
              borderBottomEndRadius: 0,
              borderBottomLeftRadius: 0,
            },
          }}>
          {/* <YourOwnComponent /> */}
          <View style={{flexDirection: 'row'}}>
            <Text style={styles.filtertext}>Filters</Text>
            <TouchableOpacity
              style={{marginLeft: 'auto', marginHorizontal: 20}}
              onPress={() => navigation.navigate('BookDetails')}>
              <Image
                style={styles.filterimage}
                source={require('../images/cross.png')}
              />
            </TouchableOpacity>
          </View>
          <View style={{flexDirection: 'row'}}>
            <View style={styles.back}>
              <TouchableOpacity style={styles.textColor}>
                <Text style={styles.filtersubtext}>Board</Text>
                <Text style={styles.filtersub2text}>Class</Text>
                <Text style={styles.filtersub2text}>Subject</Text>
                <Text style={styles.filtersub2text}>Types of Books</Text>
                <Text style={styles.filtersub2text}>Author</Text>
              </TouchableOpacity>
            </View>
            <View style={{flexDirection: 'column'}}>
              <View style={{flexDirection: 'row'}}>
                <View style={{marginHorizontal: 20, marginTop: 30}}>
                  <RadioButton
                    value="first"
                    status={checked === 'first' ? 'checked' : 'unchecked'}
                    onPress={() => setChecked('first')}
                    uncheckedColor={'#263238'}
                    color={'#FA6400'}
                  />
                </View>
                <Text style={styles.radioText}>CBSE</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <View style={{marginHorizontal: 20, marginTop: 15}}>
                  <RadioButton
                    value="second"
                    status={checked === 'second' ? 'checked' : 'unchecked'}
                    onPress={() => setChecked('second')}
                    uncheckedColor={'#263238'}
                    color={'#FA6400'}
                  />
                </View>
                <Text style={styles.radio2Text}>ICSE</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <View style={{marginHorizontal: 20, marginTop: 15}}>
                  <RadioButton
                    value="third"
                    status={checked === 'third' ? 'checked' : 'unchecked'}
                    onPress={() => setChecked('third')}
                    uncheckedColor={'#263238'}
                    color={'#FA6400'}
                  />
                </View>
                <Text style={styles.radio2Text}>Punjab Board</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <View style={{marginHorizontal: 20, marginTop: 15}}>
                  <RadioButton
                    value="fourth"
                    status={checked === 'fourth' ? 'checked' : 'unchecked'}
                    onPress={() => setChecked('fourth')}
                    uncheckedColor={'#263238'}
                    color={'#FA6400'}
                  />
                </View>
                <Text style={styles.radio2Text}>UP Board</Text>
              </View>
              <View style={{flexDirection: 'row'}}>
                <View style={{marginHorizontal: 20, marginTop: 15}}>
                  <RadioButton
                    value="fivth"
                    status={checked === 'fivth' ? 'checked' : 'unchecked'}
                    onPress={() => setChecked('fivth')}
                    uncheckedColor={'#263238'}
                    color={'#FA6400'}
                  />
                </View>
                <Text style={styles.radio2Text}>Bihar Board</Text>
              </View>
            </View>
          </View>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-evenly',
              marginHorizontal: 10,
            }}>
            <TouchableOpacity
              style={styles.filtertouch}
              onPress={() => navigation.navigate('Accountancy')}>
              <Text style={styles.filtertouchtext}>Reset</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.filtertouch2}
              onPress={() => navigation.navigate('Accountancy')}>
              <Text style={styles.filtertouch2text}>Apply</Text>
            </TouchableOpacity>
          </View>
        </RBSheet>
      </View>
      <ScrollView decelerationRate={0.5}>
        <Text style={styles.text}>Explore Books</Text>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <Text style={styles.texted}>Featured</Text>
          <Text style={styles.endText}>View all</Text>
        </View>
        <ScrollView horizontal>
          <View
            style={{
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View>
              <TouchableOpacity
                onPress={() => navigation.navigate('ExporeDetails')}>
                <Image
                  style={{
                    width: 120,
                    height: 176,
                    marginTop: 10,
                    marginHorizontal: 30,
                  }}
                  source={require('../images/book.png')}
                />
              </TouchableOpacity>
              <Text style={styles.subtext}>A Complete {`\n`}Course in…</Text>
              <Image
                style={styles.sub3image}
                source={require('../images/star.png')}
              />
              <Image
                style={styles.sub4image}
                source={require('../images/star.png')}
              />
              <Image
                style={styles.sub5image}
                source={require('../images/star.png')}
              />
            </View>

            <View style={{marginLeft: -30}}>
              <Image
                style={{
                  width: 120,
                  height: 176,
                  marginTop: 10,
                  marginHorizontal: 30,
                }}
                source={require('../images/book.png')}
              />
              <Text style={styles.subtext}>A Complete {`\n`}Course in…</Text>
              <Image
                style={styles.sub3image}
                source={require('../images/star.png')}
              />
              <Image
                style={styles.sub4image}
                source={require('../images/star.png')}
              />
              <Image
                style={styles.sub5image}
                source={require('../images/star.png')}
              />
            </View>

            <View style={{marginLeft: -30}}>
              <Image
                style={{
                  width: 120,
                  height: 176,
                  marginTop: 10,
                  marginHorizontal: 30,
                }}
                source={require('../images/book.png')}
              />
              <Text style={styles.subtext}>A Complete {`\n`}Course in…</Text>
              <Image
                style={styles.sub3image}
                source={require('../images/star.png')}
              />
              <Image
                style={styles.sub4image}
                source={require('../images/star.png')}
              />
              <Image
                style={styles.sub5image}
                source={require('../images/star.png')}
              />
            </View>
          </View>
        </ScrollView>
        <Text style={styles.texted}>Latested</Text>
        <View style={{flexDirection: 'row'}}>
          <Image
            style={styles.subImage}
            source={require('../images/book.png')}
          />
          <View>
            <Text style={styles.inputtxt}>Core Composite Mathematics</Text>
            <Text style={styles.sub2text}>
              A Complete Course In Science Lab Manual…
            </Text>
          </View>
        </View>

        <Image
          style={styles.sub6image}
          source={require('../images/star.png')}
        />
        <Image
          style={styles.sub7image}
          source={require('../images/star.png')}
        />
        <Image
          style={styles.sub8image}
          source={require('../images/star.png')}
        />

        <View
          style={{
            flexDirection: 'row',
            marginHorizontal: 60,
          }}>
          <TouchableOpacity style={styles.touch}>
            <Text style={styles.touchtext}>Preview</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.touch2}
            onPress={() => navigation.navigate('OnlinePopup')}>
            <Text style={styles.touchtext}>Activate</Text>
          </TouchableOpacity>
        </View>

        <View style={{flexDirection: 'row'}}>
          <Image
            style={styles.subImage}
            source={require('../images/book.png')}
          />
          <View>
            <Text style={styles.inputtxt}>Core Composite Mathematics</Text>
            <Text style={styles.sub2text}>
              A Complete Course In Science Lab Manual…
            </Text>
          </View>
        </View>

        <Image
          style={styles.sub6image}
          source={require('../images/star.png')}
        />
        <Image
          style={styles.sub7image}
          source={require('../images/star.png')}
        />
        <Image
          style={styles.sub8image}
          source={require('../images/star.png')}
        />

        <View
          style={{
            flexDirection: 'row',
            marginHorizontal: 60,
          }}>
          <TouchableOpacity style={styles.touch}>
            <Text style={styles.touchtext}>Preview</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.touch2}
            onPress={() => navigation.navigate('OnlinePopup')}>
            <Text style={styles.touchtext}>Activate</Text>
          </TouchableOpacity>
        </View>

        <View style={{flexDirection: 'row'}}>
          <Image
            style={styles.subImage}
            source={require('../images/book.png')}
          />
          <View>
            <Text style={styles.inputtxt}>Core Composite Mathematics</Text>
            <Text style={styles.sub2text}>
              A Complete Course In Science Lab Manual…
            </Text>
          </View>
        </View>

        <Image
          style={styles.sub6image}
          source={require('../images/star.png')}
        />
        <Image
          style={styles.sub7image}
          source={require('../images/star.png')}
        />
        <Image
          style={styles.sub8image}
          source={require('../images/star.png')}
        />

        <View
          style={{
            flexDirection: 'row',
            marginHorizontal: 60,
          }}>
          <TouchableOpacity style={styles.touch}>
            <Text style={styles.touchtext}>Preview</Text>
          </TouchableOpacity>

          <TouchableOpacity
            style={styles.touch2}
            onPress={() => navigation.navigate('OnlinePopup')}>
            <Text style={styles.touchtext}>Activate</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default ExploreBooks;

const styles = StyleSheet.create({
  image: {
    width: 60,
    height: 60,
    marginLeft: 'auto',
    marginTop: -60,
    marginHorizontal: 20,
  },
  subImage: {
    width: 76,
    height: 100,
    marginHorizontal: 30,
    marginTop: 20,
    borderRadius: 8,
  },
  text: {
    // marginTop: 20,
    marginHorizontal: 30,
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 25,
    fontWeight: 'bold',
    color: '#333333',
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 16,
    fontWeight: 'bold',
    color: '#333333',
    marginHorizontal: 30,
    marginTop: 5,
  },
  texted: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    marginTop: 20,
    color: '#1E1F20',
    marginHorizontal: 30,
  },
  endText: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 14,
    fontWeight: 'bold',
    marginTop: 20,
    color: '#FA6400',
    marginHorizontal: 20,
  },
  inputtxt: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 15,
    fontWeight: 'bold',
    color: '#333333',
    marginTop: 20,
    marginLeft: -10,
  },
  sub3image: {
    marginHorizontal: 30,
    marginTop: 5,
    height: 14,
    width: 14,
  },
  sub4image: {
    marginHorizontal: 45,
    height: 14,
    width: 14,
    marginTop: -14,
  },
  sub5image: {
    marginHorizontal: 60,
    height: 14,
    width: 14,
    marginTop: -14,
  },
  sub6image: {
    marginHorizontal: 125,
    marginTop: -55,
    height: 14,
    width: 14,
  },
  sub7image: {
    marginHorizontal: 140,
    height: 14,
    width: 14,
    marginTop: -14,
  },
  sub8image: {
    marginHorizontal: 155,
    height: 14,
    width: 14,
    marginTop: -14,
  },
  sub2text: {
    fontFamily: 'AvenirLTStd-Normal',
    fontSize: 12,
    fontWeight: '400',
    color: '#999999',
    marginTop: 3,
    marginLeft: -10,
  },
  touch: {
    width: 80,
    height: 30,
    borderRadius: 25,
    backgroundColor: '#FA6400',
    marginTop: 10,
    marginLeft: 60,
    // marginBottom: 10,
    // alignSelf: 'center',
  },
  touch2: {
    width: 80,
    height: 30,
    borderRadius: 25,
    backgroundColor: '#0253B3',
    marginTop: 10,
    marginLeft: 10,
    // alignSelf: 'center',
  },
  touchtext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 12,
    fontWeight: 'bold',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 6,
  },
  Line: {
    height: 1,
    borderRadius: 5,
    backgroundColor: '#1E1F204d',
    marginTop: 7,
    marginHorizontal: 30,
    // marginLeft: 25,
  },
  filtertext: {
    fontFamily: 'AvenirLTStd-Heavy',
    fontSize: 20,
    fontWeight: 'bold',
    color: '#263238',
    marginHorizontal: 30,
  },
  filterimage: {
    width: 25,
    height: 25,
  },
  back: {
    width: 158,
    height: 375,
    backgroundColor: '#f5e4f1',
    marginTop: 20,
  },
  textColor: {
    width: 158,
    height: 40,
    backgroundColor: '#FA6400',
    marginTop: 20,
  },
  filtersubtext: {
    color: '#fff',
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 7,
    marginHorizontal: 15,
  },
  filtersub2text: {
    color: '#263238',
    fontSize: 16,
    fontWeight: '500',
    marginTop: 20,
    marginHorizontal: 15,
  },
  radioText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#263238',
    marginTop: 33,
    marginLeft: -15,
  },
  radio2Text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 18,
    fontWeight: '500',
    color: '#263238',
    marginTop: 18,
    marginLeft: -15,
  },
  filtertouch: {
    width: 145,
    height: 50,
    borderRadius: 5,
    backgroundColor: '#6D7278',
    marginTop: '5%',
    elevation: 15,
  },
  filtertouchtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 17,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 12,
  },
  filtertouch2: {
    width: 186,
    height: 50,
    borderRadius: 5,
    backgroundColor: '#0253B3',
    marginTop: '5%',
    elevation: 15,
  },
  filtertouch2text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 17,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 10,
  },
});
