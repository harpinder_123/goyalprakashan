import React, {useEffect, useState} from 'react';
import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  Dimensions,
  TouchableOpacity,
  ImageBackground,
} from 'react-native';
import {StatusBarDark} from '../Custom/CustomStatusbar';
import {HeaderImage} from '../Custom/CustomView';
const {height} = Dimensions.get('window');

const Solutions = ({navigation}) => {
  return (
    <View style={{backgroundColor: '#FFFFFF', flex: 1}}>
      <StatusBarDark />
      <ScrollView decelerationRate={0.5}>
        <HeaderImage onPress={() => navigation.goBack()} title="Solutions" />
        <View style={styles.container}>
          <Text style={styles.containerText}>2/50</Text>
        </View>

        <Text style={styles.subtext}>
          The information for the preparation{`\n`}of receipt and payments
          account is{`\n`}taken from …
        </Text>
        <View style={styles.box2}>
          <Text style={styles.box2text}>A. Cash Book</Text>
        </View>
        <View style={styles.box3}>
          <Text style={styles.box3text}>B. Income and Expenditure A/c</Text>
        </View>
        <View style={styles.box}>
          <Text style={styles.boxtext}>C. Cash book and balance sheet</Text>
        </View>
        <View style={styles.box}>
          <Text style={styles.boxtext}>D. Revenue Account</Text>
        </View>
        <View
          style={{
            borderWidth: 1,
            backgroundColor: '#e9efe4',
            borderColor: '#6DD400',
            padding: 10,
            marginHorizontal: 30,
            marginTop: 20,
            borderRadius: 8,
          }}>
          <Text style={styles.box2text}>SOLUTION</Text>
          <Text style={styles.box4text}>
            Lorem Ipsum is simply dummy text of the printing and typesetting
            industry. Lorem Ipsum has been the industry's standard dummy text
            ever since the 1500s, when an unknown printer took a galley of type
            and scrambled it to make a type specimen book.
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-evenly',
            marginHorizontal: 10,
          }}>
          <TouchableOpacity
            style={styles.touch2}
            onPress={() => navigation.navigate('Accountancy')}>
            <Text style={styles.touch2text}>Previous</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.touch3}
            onPress={() => navigation.navigate('Accountancy')}>
            <Text style={styles.touch3text}>Next</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    </View>
  );
};

export default Solutions;

const styles = StyleSheet.create({
  container: {
    width: 50,
    height: 30,
    backgroundColor: '#EAEAEA',
    borderRadius: 5,
    marginHorizontal: 30,
    marginTop: 20,
  },
  containerText: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#273253',
    textAlign: 'center',
    marginTop: 3,
  },
  end2image: {
    width: 16,
    height: 22,
    marginHorizontal: 30,
    marginTop: 20,
    marginLeft: 'auto',
  },
  subtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 20,
    fontWeight: '500',
    color: '#273253',
    textAlign: 'justify',
    marginTop: 20,
    alignSelf: 'center',
    lineHeight: 30,
  },
  box: {
    padding: 15,
    marginHorizontal: 30,
    backgroundColor: '#ffffff',
    borderRadius: 5,
    elevation: 3,
    marginTop: 20,
  },
  box2: {
    padding: 15,
    marginHorizontal: 30,
    backgroundColor: '#ffffff',
    borderRadius: 5,
    // elevation: 3,
    marginTop: 20,
    borderColor: '#6DD400',
    borderWidth: 2,
    backgroundColor: '#edf9e1',
  },
  box3: {
    padding: 15,
    marginHorizontal: 30,
    backgroundColor: '#ffffff',
    borderRadius: 5,
    // elevation: 3,
    marginTop: 20,
    borderColor: '#E02020',
    borderWidth: 2,
    backgroundColor: '#f5dddd',
  },
  boxtext: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#333333',
    marginHorizontal: 10,
  },
  box2text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#6DD400',
    marginHorizontal: 10,
  },
  box3text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 16,
    fontWeight: '500',
    color: '#E02020',
    marginHorizontal: 10,
  },
  box4text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 14,
    fontWeight: '500',
    color: '#333333',
    marginHorizontal: 10,
    marginTop: 10,
    lineHeight: 25,
  },
  touch2: {
    width: 160,
    height: 50,
    borderRadius: 5,
    backgroundColor: '#6D7278',
    marginTop: '10%',
    elevation: 15,
  },
  touch2text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 17,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 12,
  },
  touch3: {
    width: 160,
    height: 50,
    borderRadius: 5,
    backgroundColor: '#0253B3',
    marginTop: '10%',
    elevation: 15,
    marginBottom: 20,
  },
  touch3text: {
    fontFamily: 'AvenirLTStd-Medium',
    fontSize: 17,
    fontWeight: '500',
    alignSelf: 'center',
    color: '#ffffff',
    marginTop: 12,
  },
});
